"""
  Description:
  ----
    Python is a scripting language which is executed line by line as such how we
    read.

  Functions:
  ----
    A function is a specific application which is designed to do just that alone
    Say, You want to write a code that will add two numbers and will display the
    result of the said addition of any two numbers given. Later, when moving on
    to learn more about coding if you are interested enough maybe you want to
    use the same code to add some numbers together you would have write the
    logic for the said addition again. Instead of doing that, what if there was
    a way to that without writing the same code again and again. That is were
    these functions come in help.

  Packages:
  ----
    Packages maybe defined as a group of these said functions which are directly
    associated with the building of the application.

  Projects:
  ----
    Projects are defined as the group of programs clustered together to form an
    definite application.

  Directories:
  ----
    Directories are necessarily folders containing either folders or files or
    both at the same time.

  File naming, structuring, etc:
  ----
    In Python the exists only a single type of file with the extension (.py).
    Depending on the usage it can be used as a source file or library or packages
    for programming. All the structuring, syntax-ing is/will be according to the
    PIP8 standards document.

    Link :
    ----
      https://www.python.org/dev/peps/pep-0008/

  Including Files/Modules:
  ----
    In order to include files to your own program as such given below the
    'import' is used with a file name.

    Example:
    ----
    # To import the C language operations onto python source
    import c_types

    (or)

    # To load the entire module onto the current source
    from c_types import *

  For More Information:
  ----
    Please refer the documents in the repository under the name
    Python_script_documentation.org

    Note:
    ----
      This document is an org file (.org) so you might need some special
      software to read the file properly. We do recommend the following
      software which works best in said configuration.
        1. Emacs under Spacemacs configuration Link: http://spacemacs.org/#
        2. GitLab repository viewer

    Editors & Integrated Development Environment(IDEs):
    ----

"""
"""
  Comment Lines:
  ----
    Comment lines are present in the program to assess the code written by
    the programmer. Later to development of the program itself some random
    person who's reviewing the code would easy understand code's intention.
    These comment lines doesn't affect the program in any way if properly
    terminated, like this one.
    In the below comments we'll be using single line comments for knowing
    about the types of comments available in C language, the reason is that
    the multi-line should not contain the end termination before the actual end
    of comment. So only the single comment is suited to show you the list
    below.

  Note:
  ----
    Some compilers might not support comments within comments, will output a
    warning but if you're not bothered about that then you can leave it.

  There are two types of comment statements available

  Single line comment:
  ----
    # comment here

  Multi-line comment:
  ----
    ''' comment here ''' (or) similar comment given in this message.
"""

"""
  To display a message on to the terminal/console window with the specified
  string as the message to display

  Syntax:
  ----
    print(" Your Message Here ")
    (or)
    print(' Your Message Here ')
    (or)
    print(variable_name)

  Note:
  ----
    Unlike C language there's no terminating character(;) used in Python script
    The reason being that it doesn't need any and uses the spaces between the
    keywords (at the start/middle/end).
"""
print("Hi, Welcome to Python")
print('Hi, Welcome to Python')
'''
  From the below statement you can see that "You have" is combined as 'You've'
  but printing 'You've' on the terminal will require some caveats to be
  cleared.

  Caveats
  ----
    If you trying to print a message enclosed in " Your Message Here " you
    can either use (') or (\') within the " "
    if you're using ' Your Message Here ' to print then you can use only
    (\') for (') and (") for 'You"ve'.
'''
print("You're Learning Python")
print('You\'re Learning "Python"')

"""
  To get input from the user python uses a function called input() to get the
  string of characters.
  A variable can be initialised just by assigning the values/returns/functions
  to the specified variable names.
  The variable names is quite similar to C language
"""
getting_input = input("Please enter a value as a number : ")

# Displays the inputted string of characters on the console/terminal
print(getting_input)

'''
  To displays the inputted string and also appends our own custom message to
  it.

  Method 1 : Adding variable to custom message
  ----
    This will append the variable to the message string within the print()
    function. The variables has to separated from the string itself and should
    be given/enclosed within + variable_name + to add it to the string

  Method 2 : Formatted string
  ----
    This is quite in a way similar to C language's implementation of adding
    variables to the print() function.

  Syntax:
  ----
    Method 1 :
            print(" Your Message Here " + variable_name)
      (or)  print(" Your Message Here " + variable_name + " Your Message 2 ")
    Method 2 :
            f' Your Message Here {variable_name}'
'''
# Method 1 : Adding variable to custom message
print("You\'ve entered \"" + getting_input + "\"")
print("You\'ve entered " + getting_input + " huh! ")

# Method 2 : Formatted string
print(f'You\'ve entered {getting_input}')
# Method 2.1 : Formatted string
format_string = f'You\'ve entered {getting_input}'
print(format_string)