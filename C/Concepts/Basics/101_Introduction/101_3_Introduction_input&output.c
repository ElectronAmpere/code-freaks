/**
 * Description:
 * ----
 *
 * The #include <filename.extension> is a pre-processing directive which will
 * be added to program during compilation. Pre-processing is the process which
 * is done well before the code is processed by the target CPU on which it's
 * running on. All the pre-processing keywords will be represented by the
 * prefix '#' and the values assigned to said keyword will replace the keywords
 * presence in the program before the code is processed by the target CPU.
 * The #include will include the files that is necessary for application
 * program, in this case the basic input and output manipulation file stdio.h
 * which stands the standard input and output file.
 *
 * Functions:
 * ----
 *
 * A function is a specific application which is designed to do just that alone
 * Say, You want to write a code that will add two numbers and will display the
 * result of the said addition of any two numbers given. Later, when moving on
 * to learn more about coding if you are interested enough maybe you want to
 * use the same code to add some numbers together you would have write the
 * logic for the said addition again. Instead of doing that, what if there was
 * a way to that without writing the same code again and again. That is were
 * these functions come in help.
 *
 * Programs:
 * ----
 *
 * Programs maybe defined as a group of these said functions which are directly
 * associated with the building of the application.
 *
 * Projects:
 * ----
 *
 * Projects are defined as the group of programs clustered together to form an
 * definite application.
 *
 * Directories:
 * ----
 *
 * Directories are necessarily folders containing either folders or files or
 * both at the sametime.
 *
 * File naming, structuring, etc:
 * ----
 *
 * In the C language there exists different types of file depending on their
 * extension names.
 *
 * [Headerfile.h] --->
 * The header file which is given by .h will generally contain the function
 * names with inputs that can be used in other programs.
 *
 * [Sourcefile.c] --->
 * The source file which is given by .c will generally contain the function
 * processing based on the inputs given to the function.
 *
 * [Objectfile.o] --->
 * The object file which is given by .o will generally contain the machine
 * understandable code which is binary but some may output as hexadecimals, etc
 *
 * [Archivedfile.a] --->
 * The archieved file which is given by .a will contain the basic files needed
 * to program the application code which we will be writing below. The usage of
 * these files can be understood by reading any book related to C language but
 * the actual working of the functions within these files are not visible to
 * know about them.
 *
 * Including Files:
 * ----
 *
 * In order to include files to your own program as such given below the
 * #include is used with a file name in <> in case you are using the standard
 * library files. These library files are archived files are to be written
 * within <stdio.h>. In certain cases we'll be using "" to assign files to
 * programs which indicate that these files have visible source code within
 * the project directory.
 *
 *
 *
 * For More Information:
 * ----
 * Please refer the documents in the repository under the name
 * C_Language_documentation.org
 *
 * Note:
 *    This document is an org file (.org) so you might need some special
 *    softwares to read the file properly. We do recommand the following
 *    softwares which works best in said configuration.
 *      1. Emacs under Spacemacs configuration Link: http://spacemacs.org/#
 *      2. Sublime Text in org-mode Link: https://www.sublimetext.com
 *      3. GitLab repository viewer
 */

/**
 * Comment Lines:
 * ----
 *
 * Comment lines are present in the program to access the code written by
 * the programmer. Later to development of the program itself some random
 * person who's reviewing the code would easy understand code's intention.
 * These comment lines doesn't affect the program in any way if properly
 * terminated, like this one.
 * In the below comments we'll be using single line comments for knowing
 * about the types of comments available in C language, the reason is that
 * the multi-line should not contain the end temination before the actual end
 * of comment. So only the single comment is suited to show you the list
 * below.
 *
 * Note:
 *  Some compilers might not support comments within comments will output a
 *  warning but if you're not bothered about that then you can leave it.
 */
 // There are two types of comment statements available
 // Single line comment: //comment here
 // Multi-line comment: /* */
#include <stdio.h>

/**
 * Function Definition Syntax:
 * ----
 *
 * The basic syntax of any function that's written or yet to be is given by
 * return_datatype function_name (datatype1 argument1,datatype2 argument2,
 *                                datatype3 argument3,...)
 * { start_of_function
 *    //comments
 *    statements are written here; statement_termiation
 *    returns;
 * } end_of_function
 *
 * If your wondering what does this all mean see the code below you might
 * understand.
 */
/**
 * @brief      This is called the main function of the program. It is assumed
 *             that the program starts here generally. But that is not the case
 *             in every system, in embedded system the actual code starts some
 *             other place than the main function which is congfigured in the
 *             file starup file startup.s which we'll be analysing later.
 *             The code below is called a function it has arguments as void and
 *             returns an integer.
 *
 *             The function main is said to display the information/message
 *             "Your First program" when executed/run
 *
 * @fn         int main(void)
 *
 * @param      void  The void
 *
 * @return     Returns the value zero at the end of the function execution
 *             Apparently, any value other than zero results in a error code
 *             which will also dealt with later.
 */
int main(void)
{
  /**
   * Lets consider a situation were we need to get inputs from the user while
   * the program is running. Say, we are adding any two numbers in a program
   * but we don't know the two numbers or we want to add different numbers
   * every time it runs. In order to that we need to use scanf in case of
   * normal programming but it may differ for embedded systems. In order to
   * get an input you have to use scanf() function, but to store the inputted
   * value you need a variable of a said datatype.
   */
  int   int_variable;
  float float_variable;
  char  char_variable;
  char  char_string[100];

  /**
   * The printf & scanf combination will tell the user to input the kind of
   * data recquired by the code. The printf statement will display the message
   * and the scanf will get the input and assign is to the memory of the
   * variable.
   */
  /**
   * This will get the integer input from user
   */
  printf("Enter integer value: ");
  scanf("%d",&int_variable);
  printf("Value in int_variable = %d\n",int_variable);

  /**
   * This will get the float input from user
   */
  printf("Enter float value: ");
  scanf("%f",&float_variable);
  printf("Value in float_variable = %f\n",float_variable);

  /**
   * This will get the character input from user but since enter in a single
   * input so the program will if placed at the end without visualising the
   * result. So I have added another variant to get the character input.
   */
  printf("Enter a character: ");
  scanf("%c",&char_variable);
  printf("\nValue in char_variable = %c\n",char_variable);

  /**
   * The below statement apart from the word 'word' there is but on difference
   * from before that is the format specifier %c is replaced by %s to get a
   * string input from user. A string is a series of character terminated by
   * enter or '\n' new-line from escape sequence. Since we are getting a string
   * the format specifier demands a memory location to be assigned to it so I
   * have added & prefix to the variable.
   */
  printf("Enter a word: ");
  scanf("%s",&char_variable);
  printf("Value in char_variable = %s\n",&char_variable);

  /**
   * To get the string containing \n new-line character in it.
   * You can take a string as input in C using scanf(“%s”, char_string). But, it
   * accepts string only until it finds the first space. In order to take a line
   * as input, you can use scanf("%[^\n]%*c", char_string);
   * Here, [] is the scanset character. ^\n stands for taking input until a newline
   * isn't encountered. Then, with this %*c, it reads the newline character and here,
   * the used * indicates that this newline character is discarded.
   */
  printf("Enter a sentence: ");
  scanf("%[^\n]%*c", char_string);
  printf("\nValue in char_string = %s",char_string);

  /**
   * This is the non-essential return statement for the function when specified
   * the return has to be 0 if not then you just don't care.
   */
  return (0);
}