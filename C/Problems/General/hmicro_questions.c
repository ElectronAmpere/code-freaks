#include <stdio.h>
#include <string.h>

#define DATA_LENGTH_8b        (128U)
#define DATA_LENGTH_64b       (16U)
#define DATA_LENGTH_32b       (32U)
#define DATA_LENGTH_BYTECOUNT (8U)

#define BITS_FOR_BYTE         (8U)
#define BITS_FOR_DWORD        (64U)

#define DATA_CANBUS_LENGTH    (2U)

typedef unsigned char       uint8_t;
typedef unsigned short      uint16_t;
typedef unsigned long       uint32_t;
typedef unsigned long long  uint64_t;

void gettingSubset(uint8_t* srcBuffer, uint8_t subSetDepth)
{
  /* Here the pointer must not be initialised since it can be varied anytime */
  uint8_t* subSetPointer[subSetDepth];
  uint8_t  scanVariable[subSetDepth];
  uint8_t  instanceCounter[subSetDepth];

  /* This loop can navigate to all the elements with the given array of elements */
  for (int instance = 0; instance < strlen((const char*)srcBuffer); instance++)
  {
    /* This statement prints all the elements both as a character and their respective ascii values */
    printf("srcBuffer[%d]\t= %c | 0x%hhx\n",instance,srcBuffer[instance],srcBuffer[instance]);
    if (subSetDepth != 0)
    {
      scanVariable[instance] = srcBuffer[instance];
      for (int jinstance = (instance + 1); jinstance < strlen((const char*)srcBuffer); jinstance++)
      {
        if (scanVariable[instance] == scanVariable[jinstance])
        {
          instanceCounter[instance]++;
          //printf("instanceCounter[%d]\t= %hhu | 0x%hhx\n",instance,instanceCounter[instance],instanceCounter[instance]);
        }
        else
        {
          subSetPointer[instance] = &srcBuffer[instance];
          printf("subSetPointer[%d]\t= %s\n",instance,subSetPointer[instance]);
        }
      }
      subSetDepth--;
    }
  }
}

int main()
{
  /**
   * Array. Split into unique subset of given depth. 2 deep means two unique sets.
   */
  uint8_t  mainDataBuffer[DATA_LENGTH_8b] = "#VNAME134 $VNUM6384 *64533 +87432 A7654 B6364 C6473 D6473 E9182 F7464 G6748 H2347 I9847 J1238";
  gettingSubset(mainDataBuffer,18);
  return 0;
}