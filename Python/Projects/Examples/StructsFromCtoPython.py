from struct import *
# importing array class to use array
import array as arr
import numpy as np
# from ctypes import Structure, c_uint, c_short, c_uint32, c_ulong
from ctypes import *

from dataclasses import dataclass

'''
@dataclass
class Point(Structure):
    def __init__(self, *args: Any, **kw: Any):
        super().__init__(*args, **kw)
        x: float
        y: float
        z: float = 0.0


p = Point(1.5, 2.5)

print(p)  # Point(x=1.5, y=2.5, z=0.0)
'''

# Store data in bytes
packed_data = pack('iiI', 0, 300, 65731)
print(packed_data.hex())

# Size of formats
print(calcsize('i'))
print(calcsize('I'))
print(calcsize('iiI'))

# To get bytes from packed data
original_data = unpack('iiI', packed_data)
print(original_data)



# unsigned integer array
a = arr.array ("I", [10, 256, 30, 40, 50])
# signed integer array
b = arr.array ("i", [10, -20, 30, -40, 50])

np.array(a, dtype="uint16")

print(a[1])
print(b[1])

i = -3
print(i)
ui = abs(i)
print(ui)
# for 16 bit integers
ui = (i & 0xff)
print(ui)

i = 3
print(i)
ui = abs(i)
print(ui)
# for 16 bit integers
ui = (i & 0xff)
print(ui)


"""

typedef struct{
   int16_t steer;
   int16_t speed;
   uint32_t crc;
} Serialcommand;

unsigned char       U1(c_ubyte); /*  8-bit */
unsigned short      U2(c_ushort); /* 16-bit */
unsigned long       U4(c_ulong); /* 32-bit */
unsigned long long  U6(c_ulonglong); /* 64-bit */

signed char         S1(c_byte); /*  8-bit */
signed short        S2(c_short); /* 16-bit */
signed long         S4(c_int); /* 32-bit */
signed long long    S6(c_long); /* 64-bit */

"""


# class MyStruct(Structure):
#     _fields_ = [
#         ('steer', c_short * 20),
#         ('speed', c_short * 20),
#         ('speed', c_uint32 * 20),
#     ]
# c_struct = MyStruct()
# c_struct._fields_[0] = 256
# print(f'{c_struct._fields_[0]}')


class SerialStep:
    # constructor
    def __init__(self):
        self.steer = 0
        self.speed = 0
        self.crc = 0


def CalculateCrcByte(r):
    r_local = c_ulong(r)
    # print(f'r_local = {r_local}')
    # print(f'r_local.value = {r_local.value}')
    hex_mask1 = c_ulong(0xEDB88320)
    # print(f'hex_mask1 = {hex_mask1}')
    # 3988292384
    # print(f'hex_mask1.value = {hex_mask1.value}')
    hex_mask2 = c_ulong(0xFF000000)
    # print(f'hex_mask2 = {hex_mask2}')
    # 4278190080
    # print(f'hex_mask2.value = {hex_mask2.value}')
    for instance in range(0, 8):
        r_local.value = (0 if (r_local.value & 1) else hex_mask1.value) \
                                                      ^ r_local.value >> 1
        # print(f'r_local.value in for = {r_local.value}')
    return c_ulong(r_local.value ^ hex_mask2.value)


'''
void crc32(const void *data, size_t n_bytes, uint32_t* crc) {
  static uint32_t table[0x100];
  if(!*table)
    for(size_t i = 0; i < 0x100; ++i)
      table[i] = crc32_for_byte(i);
  for(size_t i = 0; i < n_bytes; ++i)
    *crc = table[(uint8_t)*crc ^ ((uint8_t*)data)[i]] ^ *crc >> 8;
}

'''
table_crc = 0


def CalculateCrc(*data, n_bytes):
    crc = 0
    print(f'data = {data.__contains__(stepCmd)}')
    # The address of table_crc is not true
    # Replacement for @if(!*table_crc)
    if not id(table_crc):
        # Replacement for @for(size_t i = 0; i < 0x100; ++i)
        for instance in range(0, 256):
            table_crc[instance] = CalculateCrcByte(i).value
            print(f'table_crc[instance] = {table_crc[instance]}')
    for instance in range(0, n_bytes):
        crc = table_crc[crc ^ data.__getitem__([instance])] ^ crc >> 8
    return crc
    

stepCmd = SerialStep()
stepCmd.steer = 0
stepCmd.speed = 110
stepCmd.crc = 0x334

print(stepCmd.steer)
print(stepCmd.speed)
print(stepCmd.crc)

char = stepCmd.speed
# char = c_short(char)
# char = c_char_p(id(char.value))
char = c_ubyte(int(char))
print(hex(char.value))
print(c_ulong(0xEDB88320))
hecas = c_ulong(0xEDB88320)
print(hecas.value)
# 2724731650 for 5 byte crc
# 3523407757 for 0 byte crc
print(f'CalculateCrcByte = {CalculateCrcByte(0).value}')
cons = [10, 223, 165, 54, 10, 26, 6, 66, 69, 96]
print(f'CalculateCrc = {CalculateCrc(stepCmd ,n_bytes=12)}')