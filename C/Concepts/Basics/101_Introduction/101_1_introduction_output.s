	.section	__TEXT,__text,regular,pure_instructions
	.build_version macos, 10, 15	sdk_version 10, 15, 4
	.globl	_main                   ## -- Begin function main
	.p2align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
## %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movl	$0, -4(%rbp)
	leaq	L_.str(%rip), %rdi
	movb	$0, %al
	callq	_printf
	leaq	L_.str.1(%rip), %rdi
	movl	$1, %esi
	movl	%eax, -8(%rbp)          ## 4-byte Spill
	movb	$0, %al
	callq	_printf
	leaq	L_.str.2(%rip), %rdi
	movl	$1, %esi
	movl	$65, %edx
	leaq	L_.str.3(%rip), %rcx
	movl	%eax, -12(%rbp)         ## 4-byte Spill
	movb	$0, %al
	callq	_printf
	leaq	L_.str.4(%rip), %rdi
	movl	%eax, -16(%rbp)         ## 4-byte Spill
	movb	$0, %al
	callq	_printf
	leaq	L_.str.5(%rip), %rdi
	movl	$1, %esi
	movl	%eax, -20(%rbp)         ## 4-byte Spill
	movb	$0, %al
	callq	_printf
	leaq	L_.str.6(%rip), %rdi
	movl	$1, %esi
	movl	$65, %edx
	leaq	L_.str.3(%rip), %rcx
	movl	%eax, -24(%rbp)         ## 4-byte Spill
	movb	$0, %al
	callq	_printf
	leaq	L_.str.7(%rip), %rdi
	movl	$1, %esi
	movl	$65, %edx
	leaq	L_.str.3(%rip), %rcx
	movl	%eax, -28(%rbp)         ## 4-byte Spill
	movb	$0, %al
	callq	_printf
	xorl	%edx, %edx
	movl	%eax, -32(%rbp)         ## 4-byte Spill
	movl	%edx, %eax
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function
	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"Your First program"

L_.str.1:                               ## @.str.1
	.asciz	"Your first program %d"

L_.str.2:                               ## @.str.2
	.asciz	"Your first program %d %c %s"

L_.str.3:                               ## @.str.3
	.asciz	"Template"

L_.str.4:                               ## @.str.4
	.asciz	"\nYour First program"

L_.str.5:                               ## @.str.5
	.asciz	"Your first program %d\n"

L_.str.6:                               ## @.str.6
	.asciz	"Your first program %d\n %c\n %s\n"

L_.str.7:                               ## @.str.7
	.asciz	"Your first program %d\n%c\n%s\n"


.subsections_via_symbols
