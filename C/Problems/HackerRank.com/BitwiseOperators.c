/**
 * @defgroup   BITWISEOPERATORS Bitwise Operators
 *
 * @brief      This file implements Bitwise Operators.
 *
 * @author     Vigneshrajarathinam
 * @date       2020
 */

 /**
  * Objective:
  * ----
  *
  * This challenge will let you learn about bitwise operators in C.
  * Inside the CPU, mathematical operations like addition, subtraction,
  * multiplication and division are done in bit-level. To perform
  * bit-level operations in C programming, bitwise operators are used
  * which are explained below.
  *
  * Bitwise AND operator & The output of bitwise AND is 1 if the
  * corresponding bits of two operands is 1. If either bit of an operand
  * is 0, the result of corresponding bit is evaluated to 0. It is denoted
  * by &.
  *
  * Bitwise OR operator | The output of bitwise OR is 1 if at least one
  * corresponding bit of two operands is 1. It is denoted by |.
  *
  * Bitwise XOR (exclusive OR) operator ^ The result of bitwise XOR operator
  * is 1 if the corresponding bits of two operands are opposite. It is denoted by ^.
  *
  * For example, for integers 3 and 5,
  *
  * 3 = 00000011 (In Binary)
  * 5 = 00000101 (In Binary)
  *
  * AND operation        OR operation        XOR operation
  *   00000011             00000011            00000011
  * & 00000101           | 00000101          ^ 00000101
  *   ________             ________            ________
  *   00000001  = 1        00000111  = 7       00000110  = 6
  *
  * Task:
  * ----
  *
  * Given set S = {1,2,3,4,...,n}, find:
  * the maximum value of a&b which is less than a given integer k, where a and b (where a < b) are two integers from set S.
  * the maximum value of a|b which is less than a given integer k, where a and b (where a < b) are two integers from set S.
  * the maximum value of a^b which is less than a given integer k, where a and b (where a < b) are two integers from set S.
  *
  * Input Format:
  * ----
  *
  * The only line contains 2 space-separated integers, n and k, respectively.
  *
  * Constraints:
  * ----
  *
  * 1. 2 ≤ n ≤ 1000
  * 2. 2 ≤ k ≤ n
  *
  * Output Format:
  * ----
  *
  * The first line of output contains the maximum possible value of a&b.
  * The second line of output contains the maximum possible value of a|b.
  * The second line of output contains the maximum possible value of a^b.
  *
  * Sample Input 0:
  * ----
  * 5 4
  *
  * Sample Output 0:
  * ----
  *
  * 2
  * 3
  * 3
  *
  * Explanation 0:
  * ----
  *
  * n = 5, k = 4
  * S = {1,2,3,4,5}
  *
  * All possible values of a and b are:
  * 1.  a = 1, b = 2; a&b = 0; a|b = 3; a^b = 3;
  * 2.  a = 1, b = 3; a&b = 1; a|b = 3; a^b = 2;
  * 3.  a = 1, b = 4; a&b = 0; a|b = 5; a^b = 5;
  * 4.  a = 1, b = 5; a&b = 1; a|b = 5; a^b = 4;
  * 5.  a = 2, b = 3; a&b = 2; a|b = 3; a^b = 1;
  * 6.  a = 2, b = 4; a&b = 0; a|b = 6; a^b = 6;
  * 7.  a = 2, b = 5; a&b = 0; a|b = 7; a^b = 7;
  * 8.  a = 3, b = 4; a&b = 0; a|b = 7; a^b = 7;
  * 9.  a = 3, b = 5; a&b = 1; a|b = 7; a^b = 6;
  * 10. a = 4, b = 5; a&b = 4; a|b = 5; a^b = 1;
  *
  * The maximum possible value of ab that is also < (k = 4) is 2, so we print 2 on first line.
  * The maximum possible value of ab that is also < (k = 4) is 3, so we print 3 on second line.
  * The maximum possible value of ab that is also < (k = 4) is 4, so we print 4 on third line.
  */

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

/* To enable or disable debugging for local reference */
#define DEBUG_ENABLE       (1)

void calculate_the_maximum(int n, int k)
{
    /* For storing the maximum iteration needed to find the result based on the user inputs */
    int max_len = 0;

    /* Iterating to find the maximum iteration length using binomial expression */
    /**
     * The binomial expression is given by
     * k=n.     k=n
     *  ∑ (x) = ∑(n-k).
     * k=1      k=1
     * where, 2 ≤ n ≤ 1000
     *        1 ≤ k ≤ n
     * Example:
     * n = 5
     *
     * k=5
     * ∑ (x) = (5 - 1) + (5 - 2) + (5 - 3) + (5 - 4)+ (5 - 5)
     * k=1
     *       = 4 + 3 + 2 + 1 + 0 = 10 iterations.
     */
    for (int instance = k; instance > 0; instance--)
    {
        max_len += (n - instance);
    }

#if (DEBUG_ENABLE == 1)
    printf("max_len = %d\n",max_len);
#endif

    int result_array[3][max_len],
        max_value_array[3] = {0},
        a = 1,b = 2;

    // Constrints
    if((n >= 2)&&(n <= 1000)&&(k >= 2)&&(k <= n))
    {
        for (int tinstance = 1; tinstance <= max_len; tinstance++)
        {
            result_array[0][tinstance-1] = a&b;
            result_array[1][tinstance-1] = a|b;
            result_array[2][tinstance-1] = a^b;
        #if (DEBUG_ENABLE == 1)
            printf("[a = %d, b = %d]\ta&b = %d,\ta|b = %d,\ta^b = %d.\n",a,b,result_array[0][tinstance-1],result_array[1][tinstance-1],result_array[2][tinstance-1]);
        #endif
            /**
             * The below step is to constriant the counts of both a and b to be within the range of k
             * Since, both a and b cannot go over the range of n it is reduced once it reaches the limit.
             */
            b++;
            if (b > n)
            {
                a++;
                b = a+1;
            }
        }
        for (int instance = 0; instance < 3; instance++)
        {
            /**
             * Loads the initial value in the result buffer for comparison
             * to find the maximum value in it.
             */
            max_value_array[instance] = result_array[instance][0];
            for (int binstance = 1; binstance < max_len; binstance++)
            {
                /**
                 * Check whether the assigned value does not exceed the range limit of k
                 * If so, the value is reset to 0
                 */
                if (max_value_array[instance] >= k)
                {
                    max_value_array[instance] = 0;
                }
                /**
                 * Compares the value assigned to the result buffer is greater than the value in the maximum
                 * buffer, if so it gets replaced with the value in the result buffer else it retains it.
                 */
                if((max_value_array[instance] >= result_array[instance][binstance]) && (result_array[instance][binstance] < k))
                {
                    max_value_array[instance] = max_value_array[instance];
                }
                else if ((max_value_array[instance] < result_array[instance][binstance]) && (result_array[instance][binstance] < k))
                {
                    max_value_array[instance] = result_array[instance][binstance];
                }
            }
        #if (DEBUG_ENABLE == 1)
            printf("max_value_array[%d] = %d\n",instance,max_value_array[instance]);
        #endif
            printf("%d\n",max_value_array[instance]);
        }
    }
    return;
}

int main() {
    int n,k;

    /* Getting inputs from the user */
    scanf("%d %d", &n, &k);
    calculate_the_maximum(n, k);

    return 0;
}

