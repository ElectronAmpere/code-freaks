/**
 * Description:
 * ----
 *
 * The C programming language was development to make machine code more easily
 * understandable by the user who's writing it. This is supposedly a high-level
 * language, which means that most probably many humans can understand it to a
 * certain degree as it's based on the human language which is typically english.
 * This was developed because understanding and correcting the machine code is
 * relatively hard and time consuming. If you wonder what machine code is, it's
 * basically a stream of 0's and 1's in strategic sequence which represents either
 * ON or OFF. It doesn't mean that 0 is OFF or 1 is ON at times it may vary
 * drastically. Okay now back to C language, since it's written in human language
 * most of us would understand what the process is about. It doesn't mean that the
 * computer/machine will understand what you have written. In the end it just needs
 * the machine code to operate as intended by the user. So this will be converted
 * to machine code with the help of a cool software named compiler. What this does
 * is it converts what you written into machine code by performing some intelligent
 * operations like grammer checking, lexical analysis, etc. Let's see the more about
 * the compiler later on.
 * So, what do you see below will be the explaination which is given in wikipedia I
 * just added it for fun. This is the first paragraph of it
 *
 * Wikipedia: (Source)
 * ----
 *
 * C (/siː/, as in the letter c) is a general-purpose, procedural computer programming
 * language supporting structured programming, lexical variable scope, and recursion,
 * while a static type system prevents unintended operations. By design, C provides
 * constructs that map efficiently to typical machine instructions and has found
 * lasting use in applications previously coded in assembly language. Such applications
 * include operating systems and various application software for computers, from
 * super-computers to embedded systems.
 *
 * Inclusions:
 * ----
 *
 * The #include <filename.extension> is a pre-processing directive which will
 * be added to program during compilation. Pre-processing is the process which
 * is done well before the code is processed by the target CPU on which it's
 * running on. All the pre-processing keywords will be represented by the
 * prefix '#' and the values assigned to said keyword will replace the keywords
 * presence in the program before the code is processed by the target CPU.
 * The #include will include the files that is necessary for application
 * program, in this case the basic input and output manipulation file stdio.h
 * which stands the standard input and output file.
 *
 * Functions:
 * ----
 *
 * A function is a specific application which is designed to do just that alone
 * Say, You want to write a code that will add two numbers and will display the
 * result of the said addition of any two numbers given. Later, when moving on
 * to learn more about coding if you are interested enough maybe you want to
 * use the same code to add some numbers together you would have write the
 * logic for the said addition again. Instead of doing that, what if there was
 * a way to that without writing the same code again and again. That is were
 * these functions come in help.
 *
 * Programs:
 * ----
 *
 * Programs maybe defined as a group of these said functions which are directly
 * associated with the building of the application.
 *
 * Projects:
 * ----
 *
 * Projects are defined as the group of programs clustered together to form an
 * definite application.
 *
 * Directories:
 * ----
 *
 * Directories are necessarily folders containing either folders or files or
 * both at the sametime.
 *
 * File naming, structuring, etc:
 * ----
 *
 * In the C language there exists different types of file depending on their
 * extension names.
 *
 * [Headerfile.h] --->
 * The header file which is given by .h will generally contain the function
 * names with inputs that can be used in other programs.
 *
 * [Sourcefile.c] --->
 * The source file which is given by .c will generally contain the function
 * processing based on the inputs given to the function.
 *
 * [Objectfile.o] --->
 * The object file which is given by .o will generally contain the machine
 * understandable code which is binary but some may output as hexadecimals, etc
 *
 * [Archivedfile.a] --->
 * The archieved file which is given by .a will contain the basic files needed
 * to program the application code which we will be writing below. The usage of
 * these files can be understood by reading any book related to C language but
 * the actual working of the functions within these files are not visible to
 * know about them.
 *
 * Including Files:
 * ----
 *
 * In order to include files to your own program as such given below the
 * #include is used with a file name in <> in case you are using the standard
 * library files. These library files are archived files are to be written
 * within <stdio.h>. In certain cases we'll be using "" to assign files to
 * programs which indicate that these files have visible source code within
 * the project directory.
 *
 *
 *
 * For More Information:
 * ----
 * Please refer the documents in the repository under the name
 * C_Language_documentation.org
 *
 * Note:
 *    This document is an org file (.org) so you might need some special
 *    softwares to read the file properly. We do recommand the following
 *    softwares which works best in said configuration.
 *      1. Emacs under Spacemacs configuration Link: http://spacemacs.org/#
 *      2. Sublime Text in org-mode Link: https://www.sublimetext.com
 *      3. GitLab repository viewer
 */

/**
 * Comment Lines:
 * ----
 *
 * Comment lines are present in the program to assess the code written by
 * the programmer. Later to development of the program itself some random
 * person who's reviewing the code would easy understand code's intention.
 * These comment lines doesn't affect the program in any way if properly
 * terminated, like this one.
 * In the below comments we'll be using single line comments for knowing
 * about the types of comments available in C language, the reason is that
 * the multi-line should not contain the end temination before the actual end
 * of comment. So only the single comment is suited to show you the list
 * below.
 *
 * Note:
 *  Some compilers might not support comments within comments will output a
 *  warning but if you're not bothered about that then you can leave it.
 */
 // There are two types of comment statements available
 // Single line comment: //comment here
 // Multi-line comment: /* */
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

/**
 * Function Definition Syntax:
 * ----
 *
 * The basic syntax of any function that's written or yet to be is given by
 * return_datatype function_name (datatype1 argument1,datatype2 argument2,datatype3 argument3,...)
 * { start_of_function
 *    //comments
 *    statements are written here; statement_termiation
 *    returns;
 * } end_of_function
 *
 * If your wondering what does this all mean see the code below you might
 * understand.
 */
/**
 * @brief      This is called the main function of the program. It is assumed
 *             that the program starts here generally. But that is not the case
 *             in every system, in embedded system the actual code starts some
 *             other place than the main function which is congfigured in the
 *             file starup file startup.s which we'll be analysing later.
 *             The code below is called a function it has arguments as void and
 *             returns an integer.
 *
 *             The function main is said to display the information/message
 *             "Your First program" when executed/run
 *
 * @fn         int main(void)
 *
 * @param      void  The void
 *
 * @return     Returns the value zero at the end of the function execution
 *             Apparently, any value other than zero results in a error code
 *             which will also dealt with later.
 *
 * @error     error: 'main' must return 'int'
 *                void main(void)
 *            If the int main (void) or int main () is replaced with
 *            void main (void) or any other datatype is replies with an error
 *            like the above error:  statement.
 */
int main(void)
{
	int int_a,int_b;
	bool boolean = true;
	int *iptr;
	unsigned int *uptr;
	intptr_t intptr;
	uintptr_t uintptr;
	int_a = 2;
	int_b = -2;
	char text[] = "dwiejdedwdi";
	char *ptext = "busstop";
	char text1[23] = "dwiejdedwdi";
	auto int int_auto;
	printf("(int_a * int_b)/int_b = %d\n",(int_a * int_b)/int_b);
	for (int instance = 0; instance < 128; instance++)
	{
		printf("ASCII Character %d = %c\n", instance, instance);
	}
	printf("%lu\n", sizeof iptr);
	printf("%lu\n", sizeof uptr);
	printf("%lu\n", sizeof intptr);
	printf("%lu\n", sizeof uintptr);
	printf("%d\n", boolean);
	printf("int_a = %lu\n",sizeof int_a);
	printf("text = %lu\n",sizeof text);
	printf("ptext = %lu\n",sizeof ptext);
	printf("text1 = %lu\n",sizeof text1);
	printf("int_auto = %d\n",int_auto);
	return (0);
}