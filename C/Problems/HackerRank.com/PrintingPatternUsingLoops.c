/**
 * @defgroup   PRINTINGPATTERNUSINGLOOPS Printing Pattern Using Loops
 *
 * @brief      This file implements Printing Pattern Using Loops.
 *
 * @author     Vigneshrajarathinam
 * @date       2020
 */
/**
 * In this problem, you need to print the pattern of the following form containing the numbers from  to .
 *               4 4 4 4 4 4 4
 *               4 3 3 3 3 3 4
 *               4 3 2 2 2 3 4
 *               4 3 2 1 2 3 4
 *               4 3 2 2 2 3 4
 *               4 3 3 3 3 3 4
 *               4 4 4 4 4 4 4
 * Input Format
 * The input will contain a single integer .
 * Constraints
 *
 * Output Format
 * Print the pattern mentioned in the problem statement.
 * Sample Input 0
 * 2
 * Sample Output 0
 * 2 2 2
 * 2 1 2
 * 2 2 2
 * Sample Input 1
 * 5
 * Sample Output 1
 * 5 5 5 5 5 5 5 5 5
 * 5 4 4 4 4 4 4 4 5
 * 5 4 3 3 3 3 3 4 5
 * 5 4 3 2 2 2 3 4 5
 * 5 4 3 2 1 2 3 4 5
 * 5 4 3 2 2 2 3 4 5
 * 5 4 3 3 3 3 3 4 5
 * 5 4 4 4 4 4 4 4 5
 * 5 5 5 5 5 5 5 5 5
 * Sample Input 2
 * 7
 * Sample Output 2
 * 7 7 7 7 7 7 7 7 7 7 7 7 7
 * 7 6 6 6 6 6 6 6 6 6 6 6 7
 * 7 6 5 5 5 5 5 5 5 5 5 6 7
 * 7 6 5 4 4 4 4 4 4 4 5 6 7
 * 7 6 5 4 3 3 3 3 3 4 5 6 7
 * 7 6 5 4 3 2 2 2 3 4 5 6 7
 * 7 6 5 4 3 2 1 2 3 4 5 6 7
 * 7 6 5 4 3 2 2 2 3 4 5 6 7
 * 7 6 5 4 3 3 3 3 3 4 5 6 7
 * 7 6 5 4 4 4 4 4 4 4 5 6 7
 * 7 6 5 5 5 5 5 5 5 5 5 6 7
 * 7 6 6 6 6 6 6 6 6 6 6 6 7
 * 7 7 7 7 7 7 7 7 7 7 7 7 7
 */
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main()
{

    int n = 7;
    //scanf("%d", &n);
    // Complete the code to print the pattern.
    int max_dimensions = (n * 2) - 1,decision_counter = 0, element_counter = 0;
    printf("max_row = %d\n",max_dimensions);
    if ((n > 0)&&(n <= 1000))
    {
        for(int int_row = 0; int_row < max_dimensions; int_row++)
        {
            for(int int_col = 0; int_col < max_dimensions; int_col++)
            {
                element_counter++;
                /* This statement will reduce the value of n to 1 in the center of the matrix pattern */
                if((max_dimensions * max_dimensions)/2 + 1 == element_counter)
                {
                    decision_counter = -(n-1);
                }
                else if (((int_row == 0)||(int_col == 0))&&
                         ((int_row < max_dimensions - 1)||(int_col < max_dimensions - 1)))
                {
                    decision_counter = 0;
                }
                else if (((int_row > 0)||(int_col > 0))&&
                        ((int_row < max_dimensions - 1)||(int_col < max_dimensions - 1)))
                {
                    decision_counter = 0;
                }
                else
                {
                    decision_counter = 0;
                }
                printf("%d ",n + decision_counter);
                //printf("decision_counter = %d\t",decision_counter);
            }
            printf("\n");
        }
    }
    return 0;
}



