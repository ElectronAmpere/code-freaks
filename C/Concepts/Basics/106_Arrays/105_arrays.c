/* A standard include for all programs except in embedded programming */
/* This is inlcude for basic input and output control and management */
#include <stdio.h>

/* #define(s) will replace the value assigned to them in place of their
   define_name in pre-processing */
/* The length of the array is given by this define */
#define ARRAY_LENGTH  (10)

/* Function decleration */
/* Declaring a function to read all the array elements bassed on the given
   array and it's specified maximum length */
/**
 * @brief      To get the elements of the array
 *
 * @param      pint_array       The pint array
 * @param      int_arraylength  The int arraylength
 *
 * @return     void
 */
void arrayGetElements(int* pint_array,int int_arraylength);

/**
 * @brief      To get array elements with recursion
 *
 * @param      pint_array        The pint array
 * @param      int_arraylength   The int arraylength
 * @param      int_instance      The int instance
 *
 * @return     void
 */
void arrayGetElementsRecursive(int* pint_array,int int_arraylength,int int_instance);
void multiArrayGetElementsRecursive(int (*pint_array)[],int arraylength,int int_instance , int int_jinstance);


/* This is the main function were the program starts, but not in the case of
   embedded systems */
/**
 * @brief      Main function
 *
 * @return     integer returns 0 always
 */
int main()
{
  /* Declaring an integer array of length n which will be determined by the
     define ARRAY_LENGTH */
  /* The buffers are referred to locations that are accessed by the array to
     store data */
  /* Error Note:
     If an array is declared without the length of the array then an error is
     presented */
  int int_array[ARRAY_LENGTH];
  int int_array1[3][3]= {{10, 20, 132},{30, 60, 1233},{50, 90, 2434}};
  #if 0
  /* Initialising values to buffer elements  */
  /* Method: 1 - Looping */
  /* Loop are instructions to the processor to iterate over a said condition
     within closed parameters */
  for(int int_instance = 0; int_instance < ARRAY_LENGTH; int_instance++)
  {
    /* The printf statement will print the strings given with "string"
       on to the computer screen */
    /* Note:
       This only works on our PCs, Macs, etc but not on your embedded
       systems */
    printf("Enter a value to be stored in array index %d = ",int_instance);

    /* The scanf statement will get the input from the user via console/
       terminal */
    /* Note:
       This only works on our PCs, Macs, etc but not on your embedded
       systems */
    scanf("%d",&int_array[int_instance]);

    /* To print new-line */
    printf("\n");
  }

  /* Function call  */
  /* The argument int_array is passed to the function as a pointer of type
     int */
  arrayGetElements(int_array,ARRAY_LENGTH);

  /* The argument int_array is passed to the function as a pointer of type
     int and the initial instance to start with */
  arrayGetElementsRecursive(int_array,ARRAY_LENGTH,0);
  #endif
  multiArrayGetElementsRecursive(int_array1, 9, 0, 0);

  /* Basic return for main function */
  return (0);
}


 /**
  * @function   arrayGetElements(void* pint_array,int int_arraylength)
  *
  * @brief      To get the array elements from the user
  *
  * @param      pint_array       The pint array
  * @param      int_arraylength  The int arraylength
  *
  * @return     void
  */
void arrayGetElements(int* pint_array,int int_arraylength)
{
  /* Initialising values to buffer elements  */
  /* Method: 1 - Looping */
  /* Loop are instructions to the processor to iterate over a said condition
     within closed parameters */
  for(int int_instance = 0; int_instance < int_arraylength; int_instance++)
  {
    /* The printf statement will print the strings given with "string"
       on to the computer screen */
    /* Note:
       This only works on our PCs, Macs, etc but not on your embedded
       systems */
    printf("Enter a value to be stored in array index %d = ",int_instance);

    /* The scanf statement will get the input from the user via console/
       terminal */
    /* Note:
       This only works on our PCs, Macs, etc but not on your embedded
       systems */
    scanf("%d",(pint_array+int_instance));

    /* To print new-line */
    printf("\n");
  }
}

 /**
  * @brief      To get elements of the array without using any king of looping
  *             statements
  *
  * @param      pint_array    The pint array
  * @param      arraylength   The arraylength
  * @param      int_instance  The int instance
  *
  * @return     void
  */
void arrayGetElementsRecursive(int* pint_array,int arraylength,int int_instance)
{
  if (int_instance < arraylength)
  {
    /* The printf statement will print the strings given with "string"
       on to the computer screen */
    /* Note:
       This only works on our PCs, Macs, etc but not on your embedded
       systems */
    printf("Enter a value to be stored in array index %d = ",int_instance);

    /* The scanf statement will get the input from the user via console/
       terminal */
    /* Note:
       This only works on our PCs, Macs, etc but not on your embedded
       systems */
    scanf("%d",(pint_array+int_instance));

    /* Increment the instance of the array index */
    int_instance++;

    /* Function call recursive */
    arrayGetElementsRecursive(pint_array,arraylength,int_instance);
  }
  else
  {
    /* To print new-line */
    printf("\n");
    return;
  }
}

 /**
  * @brief      To get elements of the array without using any king of looping
  *             statements
  *
  * @param      pint_array    The pint array
  * @param      arraylength   The arraylength
  * @param      int_instance  The int instance
  *
  * @return     void
  */
void multiArrayGetElementsRecursive(int (*pint_array)[],int arraylength,int int_instance , int int_jinstance)
{
  if (int_instance < arraylength)
  {
    /* The printf statement will print the strings given with "string"
       on to the computer screen */
    /* Note:
       This only works on our PCs, Macs, etc but not on your embedded
       systems */
    //printf("Enter a value to be stored in array index %d = ",int_instance);
    printf("%d = ",int_instance);
    printf("%d = ",int_jinstance);

    /* The scanf statement will get the input from the user via console/
       terminal */
    /* Note:
       This only works on our PCs, Macs, etc but not on your embedded
       systems */
    if (int_jinstance < arraylength)
    {
      printf("%d, ",(*(*pint_array+int_instance)+int_jinstance));

      int_jinstance++;
      multiArrayGetElementsRecursive(pint_array,arraylength,int_instance,int_jinstance);
    }
    else
    {}

    /* Increment the instance of the array index */
    int_instance++;

    /* Function call recursive */
    multiArrayGetElementsRecursive(pint_array,arraylength,int_instance,int_jinstance);
  }
  else
  {
    /* To print new-line */
    printf("\n");
    return;
  }
}
