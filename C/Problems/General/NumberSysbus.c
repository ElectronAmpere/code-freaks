#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MINIMALIST_MEMORY     (0)
#define MINIMALIST_CODE       (1)
#define SELECT_VERSION        (MINIMALIST_CODE)

#if (SELECT_VERSION == MINIMALIST_MEMORY)
    void NumberSysBus_Minimalist_Memory(int range);
#elif (SELECT_VERSION == MINIMALIST_CODE)
    #define SIZE_OF_STRING     (6)
    void NumberSysBus_Minimalist_Code(int range);
#endif

int main()
{
    int range = 15;
    //scanf("%d",range);
    #if (SELECT_VERSION == MINIMALIST_MEMORY)
        NumberSysBus_Minimalist_Memory(range);
    #elif (SELECT_VERSION == MINIMALIST_CODE)
        NumberSysBus_Minimalist_Code(range);
    #endif

    return (0);
}

#if (SELECT_VERSION == MINIMALIST_MEMORY)
    /**
     * @brief      { function_description }
     *
     * @param      range  The range
     *
     * @return     { description_of_the_return_value }
     */
    void NumberSysBus_Minimalist_Memory(int range)
    {
        for (int i = 0; i <= range; i++)
        {
            if (i > 0)
            {
                if (i%3 == 0)
                {
                    printf("sys");
                }
                if (i%5 == 0)
                {
                    printf("bus");
                }
                if ((i%3 != 0) && (i%5 != 0))
                {
                    printf("%d",i);
                }
                printf("\n");
            }
            else
            {
                printf("%d\n",i);
            }
        }
    }
#elif (SELECT_VERSION == MINIMALIST_CODE)
    /**
     * @brief      { function_description }
     *
     * @param      range  The range
     *
     * @return     { description_of_the_return_value }
     */
    void NumberSysBus_Minimalist_Code(int range)
    {
        /**
         * Dynamic memory allocation for the output string
         */
        char *OutputString = malloc(SIZE_OF_STRING * sizeof(char));
        /**
         * Loop based on the range set
         */
        for (unsigned char instance = 1; instance <= range; instance++)
        {
            memset(OutputString,0,strlen(OutputString));
            if (instance % 3 == 0) { strcat(OutputString,"sys"); }
            if (instance % 5 == 0) { strcat(OutputString,"bus"); }
            if (strcmp(OutputString,"\0") == 0) { printf("%d\n",instance); }
            else { printf("%s\n",OutputString); }
        }
        /**
         * Free-ing memory from allocation
         */
        free(OutputString);
    }
#endif