#include <stdio.h>
int main ()
{
  int a = 66, b = 34;

  /* Initial Values */
  printf("A:%d\n",a);
  printf("B:%d\n",b);

  /* Swapping */
  b = a + b; /* 44(b) = 10(a) + 34(b) */
  a = b - a; /* 34(a) = 44(b) - 10(a) */
  b = b - a; /* 10(b) = 44(b) - 34(a) */

  /* Swapped Values */
  printf("A:%d\n",a);
  printf("B:%d\n",b);

  return 0;
}