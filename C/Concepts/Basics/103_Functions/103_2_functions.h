/**
 * Description:
 * ----
 *
 * The #include <filename.extension> is a pre-processing directive which will
 * be added to program during compilation. Pre-processing is the process which
 * is done well before the code is processed by the target CPU on which it's
 * running on. All the pre-processing keywords will be represented by the
 * prefix '#' and the values assigned to said keyword will replace the keywords
 * presence in the program before the code is processed by the target CPU.
 * The #include will include the files that is necessary for application
 * program, in this case the basic input and output manipulation file stdio.h
 * which stands the standard input and output file.
 *
 * Functions:
 * ----
 *
 * A function is a specific application which is designed to do just that alone
 * Say, You want to write a code that will add two numbers and will display the
 * result of the said addition of any two numbers given. Later, when moving on
 * to learn more about coding if you are interested enough maybe you want to
 * use the same code to add some numbers together you would have write the
 * logic for the said addition again. Instead of doing that, what if there was
 * a way to that without writing the same code again and again. That is were
 * these functions come in help.
 *
 * Programs:
 * ----
 *
 * Programs maybe defined as a group of these said functions which are directly
 * associated with the building of the application.
 *
 * Projects:
 * ----
 *
 * Projects are defined as the group of programs clustered together to form an
 * definite application.
 *
 * Directories:
 * ----
 *
 * Directories are necessarily folders containing either folders or files or
 * both at the sametime.
 *
 * File naming, structuring, etc:
 * ----
 *
 * In the C language there exists different types of file depending on their
 * extension names.
 *
 * [Headerfile.h] --->
 * The header file which is given by .h will generally contain the function
 * names with inputs that can be used in other programs.
 *
 * [Sourcefile.c] --->
 * The source file which is given by .c will generally contain the function
 * processing based on the inputs given to the function.
 *
 * [Objectfile.o] --->
 * The object file which is given by .o will generally contain the machine
 * understandable code which is binary but some may output as hexadecimals, etc
 *
 * [Archivedfile.a] --->
 * The archieved file which is given by .a will contain the basic files needed
 * to program the application code which we will be writing below. The usage of
 * these files can be understood by reading any book related to C language but
 * the actual working of the functions within these files are not visible to
 * know about them.
 *
 * Including Files:
 * ----
 *
 * In order to include files to your own program as such given below the
 * #include is used with a file name in <> in case you are using the standard
 * library files. These library files are archived files are to be written
 * within <stdio.h>. In certain cases we'll be using "" to assign files to
 * programs which indicate that these files have visible source code within
 * the project directory.
 *
 *
 *
 * For More Information:
 * ----
 * Please refer the documents in the repository under the name
 * C_Language_documentation.org
 *
 * Note:
 *    This document is an org file (.org) so you might need some special
 *    softwares to read the file properly. We do recommand the following
 *    softwares which works best in said configuration.
 *      1. Emacs under Spacemacs configuration Link: http://spacemacs.org/#
 *      2. Sublime Text in org-mode Link: https://www.sublimetext.com
 *      3. GitLab repository viewer
 */

/**
 * Comment Lines:
 * ----
 *
 * Comment lines are present in the program to access the code written by
 * the programmer. Later to development of the program itself some random
 * person who's reviewing the code would easy understand code's intention.
 * These comment lines doesn't affect the program in any way if properly
 * terminated, like this one.
 * In the below comments we'll be using single line comments for knowing
 * about the types of comments available in C language, the reason is that
 * the multi-line should not contain the end temination before the actual end
 * of comment. So only the single comment is suited to show you the list
 * below.
 *
 * Note:
 *  Some compilers might not support comments within comments will output a
 *  warning but if you're not bothered about that then you can leave it.
 */
 // There are two types of comment statements available
 // Single line comment: //comment here
 // Multi-line comment: /* */

/**
 * @brief      The preprocessing directive #ifndef will define the said #define
 *             here if it is not defined elsewhere. This is used to stop
 *             re-defining the same header file every time it's invoked or
 *             included in code.
 *
 * @return     None
 */
#ifndef __102_2_FUNCTIONS_H__
#define __102_2_FUNCTIONS_H__

/**
 * The functions that are present here can be use by other source files if
 * invoked properly.
 */
/**
 * Function Type: 5
 * ----
 *
 * The function decleration and function definition are in seperate files.
 */
/**
 * @brief      To divide the dividend by the divisor
 *
 * @param      int_dividend  The int dividend
 * @param      int_divisor   The int divisor
 *
 * @return     The actual divided value
 */
float functionsDivTwoInt (int int_dividend, int int_divisor);
/**
 * @brief      To find the tenth place of the integer
 *
 * @param      int_variable  The integer variable
 *
 * @return     Tenth place digit
 */
int funtionsFindTenthPlaceDigit (int int_variable);

/**
 * Function Type: 6
 * ----
 *
 * The function decleration and function definition are in the same location
 * within the file.
 * The inline keyword will define and declare the function in the same memory
 * unlike other type of declerations and definitions
 * In this type of function if the function doesn't contain static keyword along
 * with inline keyword as stated below it'll output a compiler error due to
 * compiler optimisation.
 *
 * This is one of the side effect of GCC the way it handle inline function.
 * When compiled, GCC performs inline substitution as the part of optimisation.
 * So there is no function call present (foo) inside main. Please check below
 * assembly code which compiler will generate. If you see there's not function
 * call relating to functionsMultiTwoInt() in non-static version.
 *
 * Without static keyword:
 * ----
 * Error code:
 * Undefined symbols for architecture x86_64:
 * "_functionsMultiTwoInt", referenced from:
 *    _main in 102_functions-1190b1.o
 * ld: symbol(s) not found for architecture x86_64
 * error: linker command failed with exit code 1 (use -v to see invocation)
 *
 * With static keyword:
 * ----
 * refer : static.txt
 */
/**
 * @brief      To multiply two integers
 *
 * @param      int_multiplicant  The int multiplicant
 * @param      int_multiplier    The int multiplier
 *
 * @return     Multiplication result
 */
static inline int functionsMultiTwoInt (int int_multiplicant, int int_multiplier)
{
  /**
   * Returns the multiplication result
   */
  return (int_multiplicant * int_multiplier);
}

/**
 * Function Type: 7
 * ----
 *
 * The function pointer decleration.
 */
typedef int (*functionPointerIntType) (int);

#endif /* __102_2_FUNCTIONS_H__ */