/**
 * Description:
 * ----
 *
 * The #include <filename.extension> is a pre-processing directive which will
 * be added to program during compilation. Pre-processing is the process which
 * is done well before the code is processed by the target CPU on which it's
 * running on. All the pre-processing keywords will be represented by the
 * prefix '#' and the values assigned to said keyword will replace the keywords
 * presence in the program before the code is processed by the target CPU.
 * The #include will include the files that is necessary for application
 * program, in this case the basic input and output manipulation file stdio.h
 * which stands the standard input and output file.
 *
 * Functions:
 * ----
 *
 * A function is a specific application which is designed to do just that alone
 * Say, You want to write a code that will add two numbers and will display the
 * result of the said addition of any two numbers given. Later, when moving on
 * to learn more about coding if you are interested enough maybe you want to
 * use the same code to add some numbers together you would have write the
 * logic for the said addition again. Instead of doing that, what if there was
 * a way to that without writing the same code again and again. That is were
 * these functions come in help.
 *
 * Programs:
 * ----
 *
 * Programs maybe defined as a group of these said functions which are directly
 * associated with the building of the application.
 *
 * Projects:
 * ----
 *
 * Projects are defined as the group of programs clustered together to form an
 * definite application.
 *
 * Directories:
 * ----
 *
 * Directories are necessarily folders containing either folders or files or
 * both at the sametime.
 *
 * File naming, structuring, etc:
 * ----
 *
 * In the C language there exists different types of file depending on their
 * extension names.
 *
 * [Headerfile.h] --->
 * The header file which is given by .h will generally contain the function
 * names with inputs that can be used in other programs.
 *
 * [Sourcefile.c] --->
 * The source file which is given by .c will generally contain the function
 * processing based on the inputs given to the function.
 *
 * [Objectfile.o] --->
 * The object file which is given by .o will generally contain the machine
 * understandable code which is binary but some may output as hexadecimals, etc
 *
 * [Archivedfile.a] --->
 * The archieved file which is given by .a will contain the basic files needed
 * to program the application code which we will be writing below. The usage of
 * these files can be understood by reading any book related to C language but
 * the actual working of the functions within these files are not visible to
 * know about them.
 *
 * Including Files:
 * ----
 *
 * In order to include files to your own program as such given below the
 * #include is used with a file name in <> in case you are using the standard
 * library files. These library files are archived files are to be written
 * within <stdio.h>. In certain cases we'll be using "" to assign files to
 * programs which indicate that these files have visible source code within
 * the project directory.
 *
 *
 *
 * For More Information:
 * ----
 * Please refer the documents in the repository under the name
 * C_Language_documentation.org
 *
 * Note:
 *    This document is an org file (.org) so you might need some special
 *    softwares to read the file properly. We do recommand the following
 *    softwares which works best in said configuration.
 *      1. Emacs under Spacemacs configuration Link: http://spacemacs.org/#
 *      2. Sublime Text in org-mode Link: https://www.sublimetext.com
 *      3. GitLab repository viewer
 */

/**
 * Comment Lines:
 * ----
 *
 * Comment lines are present in the program to access the code written by
 * the programmer. Later to development of the program itself some random
 * person who's reviewing the code would easy understand code's intention.
 * These comment lines doesn't affect the program in any way if properly
 * terminated, like this one.
 * In the below comments we'll be using single line comments for knowing
 * about the types of comments available in C language, the reason is that
 * the multi-line should not contain the end temination before the actual end
 * of comment. So only the single comment is suited to show you the list
 * below.
 *
 * Note:
 *  Some compilers might not support comments within comments will output a
 *  warning but if you're not bothered about that then you can leave it.
 */
 // There are two types of comment statements available
 // Single line comment: //comment here
 // Multi-line comment: /* */

/**
 * Function Type: 4
 * ----
 *
 * Since the definition is above all the function where the first call will
 * be you will not get any error. Since the function doesn't contain any
 * input output statements which require #include <stdio.h> the function
 * will not have any error.
 */
/**
 * @brief      To divide the dividend by the divisor
 * @param[in]  int_dividend  The int dividend
 * @param[in]  int_divisor   The int divisor
 *
 * @return     Reminder
 */
int functionsDivTwoIntReminder (int int_dividend, int int_divisor)
{
  /**
   * Returns the reminder of the division.
   * say 7/2 = 3.5 where 3 is the quotient and 1 is the reminder
   * In order to get the quotient alone see functionsDivTwoIntQuotient()
   */
  return (int_dividend % int_divisor);
}

#include <stdio.h>
#include "103_2_functions.h"

/**
 * Function Decleration Syntax:
 * ----
 *
 * The basic syntax for any function decleration that's going to be is given by
 *
 * Syntax:
 * ----
 *
 * return_datatype function_name (datatype1 argument1,datatype2 argument2,
 *                                datatype3 argument3,...);
 *
 * Note:
 *  A function decleration can be given anywhere in the code but before any
 *  it's call and before it's definition in the program.
 */
/**
 * Function Type: 1
 * ----
 *
 * The function decleration and function definition are seperate.
 */

/**
 * Function Decleration:
 * ----
 */
/**
 * @brief      To sum integer variables and returns the result
 *
 * @param[in]  int_variable1  The int variable 1
 * @param[in]  int_variable2  The int variable 2
 *
 * @return     Sum of the two variables
 */
int functionsFindSumOfTwoInt (int int_variable1, int int_variable2);

/**
 * Function Definition:
 * ----
 */
/**
 * @brief      To sum integer variables and returns the result
 *
 * @param[in]  int_variable1  The int variable 1
 * @param[in]  int_variable2  The int variable 2
 *
 * @return     The Sum
 */
int functionsFindSumOfTwoInt (int int_variable1, int int_variable2)
{
  /**
   * Returns the of summing the two variables
   */
  return (int_variable1+int_variable2);
}

/**
 * Function Type: 2
 * ----
 *
 * There's no function decleration since it's defined just above it's first
 * usage in the program
 */
/**
 * @brief      To find the difference between two integers
 *
 * @param[in]  int_variable1  The int variable 1
 * @param[in]  int_variable2  The int variable 2
 *
 * @return     The difference
 */
int functionsFindDiffTwoInt (int int_variable1, int int_variable2)
{
  /**
   * Variable:
   * ----
   *
   * A variable is location to store information necessary to process in a
   * given program.
   * The variables declared in the function definition will only be accessable
   * only within that function. If accessed compilation errors will occur.
   */
  int int_functionresult;

  /**
   * If - else statement:
   * ----
   *
   * The if-else statement is pretty much self explanatory, there is an if
   * condition which must be held true in order to enter the statement or
   * else, the else statement.
   *
   * Syntax:
   * ----
   *
   * if-else:
   * -----
   *
   * if (relational condition only)
   * {
   *   statements;
   *   .
   *   .
   *   .
   * }
   * else
   * {
   *   statements;
   *   .
   *   .
   *   .
   * }
   *
   * if-else if-else;
   * ----
   *
   * if (relational condition only)
   * {
   *   statements;
   *   .
   *   .
   *   .
   * }
   * else if (relational condition only)
   * {
   *   statements;
   *   .
   *   .
   *   .
   * }
   * else
   * {
   *   statements;
   *   .
   *   .
   *   .
   * }
   *
   * Note:
   * ----
   *
   *  The if statement can be used alone but the else cannot exists alone
   *  without an if statement.
   *  The if statement can also be proceeded by an else if statement.
   *
   */
  /**
   * Here, the condition that's checked by the if statement is whether the
   * int_variable1 is greater than or equal to int_variable2 and in the else
   * statement will be implied that int_variable2 is greater than int_variable1
   * so the below statement make sense.
   */
  if (int_variable1 >= int_variable2)
  {
    int_functionresult = int_variable1 - int_variable2;
  }
  else
  {
    int_functionresult = int_variable2 - int_variable1;
  }

  /**
   * Returns the of subtracted value from the two variables
   */
  return (int_functionresult);
}

/**
 * Function Type: 3
 * ----
 *
 * There's function decleration at the end of the main function, since the
 * decleration is above the main function where the first call will be you will
 * not get any error.
 */
/**
 * @brief      To divide the dividend by the divisor
 *
 * @param[in]  int_dividend  The int dividend
 * @param[in]  int_divisor   The int divisor
 *
 * @return     Quotient for divided value
 */
int functionsDivTwoIntQuotient (int int_dividend, int int_divisor);

/**
 * Function Definition Syntax:
 * ----
 *
 * The basic syntax of any function that's written or yet to be is given by
 *
 * Syntax:
 * ----
 *
 * return_datatype function_name (datatype1 argument1,datatype2 argument2,
 *                                datatype3 argument3,...)
 * { start_of_function
 *    //comments
 *    statements are written here; statement_termiation
 *    returns;
 * } end_of_function
 *
 * If your wondering what does this all mean see the code below you might
 * understand.
 */
/**
 * @brief      This is called the main function of the program. It is assumed
 *             that the program starts here generally. But that is not the case
 *             in every system, in embedded system the actual code starts some
 *             other place than the main function which is congfigured in the
 *             file starup file startup.s which we'll be analysing later.
 *             The code below is called a function it has arguments as void and
 *             returns an integer.
 *
 *             The function main is said to display the information/message
 *             "Your First program" when executed/run
 *
 * @fn         int main(void)
 *
 * @param      void  The void
 *
 * @return     Returns the value zero at the end of the function execution
 *             Apparently, any value other than zero results in a error code
 *             which will also dealt with later.
 */
/**
 * @brief      In an essence we the main is in itself a function
 *
 * @return     zero or null
 */
int main(void)
{
  /**
   * @brief      functionsIntValueOfChar converts the input character to integer
   *
   * @param[in]  char_character  The character character
   *
   * @return     integer value
   */
  int functionsIntValueOfChar(char char_character);
  /**
   * Prints the sum of two integers
   *
   * Note:
   *  Functions can also be called directly within another function since it
   *  returns are relative to the format specifier.
   *  In this case integer %d replacing the need for additional variable(s)
   */
  printf("Summation      of two integers\t:\t%d\n",functionsFindSumOfTwoInt(10,23));
  printf("Difference     of two integers\t:\t%d\n",functionsFindDiffTwoInt(10,23));
  printf("Difference     of two integers\t:\t%d\n",functionsFindDiffTwoInt(233,-23));
  printf("Quotient       of two integers\t:\t%d\n",functionsDivTwoIntQuotient(7,2));
  printf("Reminder       of two integers\t:\t%d\n",functionsDivTwoIntReminder(7,2));
  printf("Division       of two integers\t:\t%f\n",functionsDivTwoInt(7,2));

  /**
   * Here, the format specifier is appended with 0.2 to %0.2f or %.2f to
   * specify the number of decimals place digits to be displayed as result
   * in the print statement.
   */
  printf("Division       of two integers\t:\t%.2f\n",functionsDivTwoInt(7,2));
  printf("Multiplication of two integers\t:\t%d\n",functionsMultiTwoInt(7,2));
  printf("Multiplication of two integers\t:\t%d\n",functionsMultiTwoInt(7,-2));

  /**
   * Initialising the function pointer variable to point to function of the
   * same type as pointer.
   * Instead of calling the function directly this can be used as a common
   * method for scheduling purposes and such
   */
  functionPointerIntType pointerToIntFunction;
  /**
   * Assigning a function address to a function pointer. The address of any
   * variable can be assigned to another by prefixing the said variable with
   * '&' when assigning to another.
   */
  pointerToIntFunction = &funtionsFindTenthPlaceDigit;
  printf("Tenth place digit of an integer\t:\t%d\n",pointerToIntFunction(26));

  /**
   * Here the function decleration is just above the usage of the function (call)
   */
  printf("Integer value of character \t:\t%d\n",functionsIntValueOfChar('A'));

  /**
   * This is the non-essential return statement for the function when specified
   * the return has to be 0 if not then you just don't care.
   */
  return (0);
}

int functionsDivTwoIntQuotient (int int_dividend, int int_divisor)
{
  /**
   * Returns the quotient of the division.
   * say 7/2 = 3.5 where 3 is the quotient and 1 is the reminder
   * In order to get the reminder alone see functionsDivTwoIntReminder()
   */
  return (int_dividend/int_divisor);
}

/**
 * @brief      To divide the dividend by the divisor
 *
 * @param      int_dividend  The int dividend
 * @param      int_divisor   The int divisor
 *
 * @return     The actual divided value
 */
float functionsDivTwoInt (int int_dividend, int int_divisor)
{
  /**
   * Returns the actual divided value
   * Say, 7/2 = 3.5 returns 3.5 exactly
   */
  /**
   * Here, the (float) is called a typecast. It is used to convert a variables
   * datatype to another datatype temporarily during it's invocation.
   * As invoked below the int_dividend and int_divisor are temporarily changed
   * to floating point variables.
   */
  return (((float)int_dividend/(float)int_divisor));
}

/**
 * @brief      To find the tenth place of the integer
 *
 * @param      int_variable  The integer variable
 *
 * @return     Tenth place digit
 */
int funtionsFindTenthPlaceDigit (int int_variable)
{
  /**
   * Returns the last digit i.e in the tenth place of the integer
   */
  return (int_variable/10);
}

/**
 * Function Type: 8
 * ----
 *
 * The function is defined in c but the decleration will be located in main()
 */
int functionsIntValueOfChar(char char_character)
{
  return((int)char_character);
}