#include <stdio.h>
#include <stdlib.h>
/**
 * The structure node will enable users to create nodes as needed.
 * The members of the node will provide the data & information
 * about the said node at any given point.
 */
struct node
{
  int   int_variable;     /* integer variable */
  char  char_variable;    /* character variable */
  float float_variable;   /* floating variable */
  struct node *next_node; /* pointer to next node */
};

/* Boolean Status */
#define TRUE  (1)
#define FALSE (1)

/**
 * @brief      Insert a new element or modify the existing element
 *             using the function InsertModifyElement(@params);
 *
 * @param[in]      data       The data
 * @param[in]      position   The position
 * @param[out]     dest_node  The destination node
 *
 * @return     There's not return statement in this function
 */
void LinkedListInsertModifyElement(int data,int position,struct node *dest_node)
{
   (dest_node+position)->int_variable  = data;
}

void LinkedListCreateNewNode(struct node *new_node)
{
  new_node = (struct node*)malloc(sizeof(struct node));
}
/**
 * @brief      The main part of the program which is executed first
 *
 * @return     returns a integer mostly zero
 */
int main()
{
  /* Initialising the root for list */
  struct node *root_node;

  /* Now root points to a node struct */
  root_node = (struct node*)malloc(sizeof(struct node));
  LinkedListCreateNewNode(root_node);
  root_node->int_variable       = 20;

  //root_node->next_node = (struct node*)malloc(sizeof(struct node));
  LinkedListCreateNewNode(root_node->next_node);
  root_node->next_node          = (root_node+1);
  (root_node+1)->int_variable   = 30;
  (root_node+1)->next_node      = (struct node*)malloc(sizeof(struct node));
  LinkedListCreateNewNode((root_node+1)->next_node);
  (root_node+1)->next_node      = (root_node+2);
  (root_node+2)->next_node      = (struct node*)malloc(sizeof(struct node));
  LinkedListCreateNewNode((root_node+2)->next_node);
  (root_node+2)->int_variable   = 40;
  (root_node+2)->next_node      = (root_node+3);
  (root_node+3)->next_node      = (struct node*)malloc(sizeof(struct node));
  LinkedListCreateNewNode((root_node+3)->next_node);
  (root_node+3)->int_variable   = 50;
  (root_node+3)->next_node      = (root_node+4);
  (root_node+4)->next_node      = (struct node*)malloc(sizeof(struct node));
  LinkedListCreateNewNode((root_node+4)->next_node);
  (root_node+4)->int_variable   = 50;
  (root_node+4)->next_node      = NULL;
  //LinkedListCreateNewNode((root_node+4));

  //LinkedListInsertModifyElement(70,3,root_node);
  //LinkedListInsertModifyElement(70,4,root_node);


  printf("Root Node\n\t-Data\t:%d\n",root_node->int_variable);
  printf("Next Node\n\t-Data\t:%d\n",root_node->next_node->int_variable);


  printf("Root Node+1\n\t-Data\t:%d\n",(root_node+1)->int_variable);
  printf("Next Node+1\n\t-Data\t:%d\n",(root_node+1)->next_node->int_variable);


  printf("Root Node+2\n\t-Data\t:%d\n",(root_node+2)->int_variable);
  printf("Next Node+2\n\t-Data\t:%d\n",(root_node+2)->next_node->int_variable);

  printf("Root Node+3\n\t-Data\t:%d\n",(root_node+3)->int_variable);
  printf("Next Node+3\n\t-Data\t:%d\n",(root_node+3)->next_node->int_variable);

  printf("Root Node+4\n\t-Data\t:%d\n",(root_node+4)->int_variable);
  printf("Next Node+4\n\t-Data\t:%d\n",(root_node+4)->next_node->int_variable);


  return 0;
}