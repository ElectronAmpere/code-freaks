#include <stdio.h>
//#include <string.h>
#define MAX_LENGTH (10)

typedef unsigned char       uint8_t; /*  8-bit */
typedef unsigned short      uint16_t; /* 16-bit */
typedef unsigned long       uint32_t; /* 32-bit */
typedef unsigned long long  U6; /* 64-bit */
typedef signed char         int8_t; /*  8-bit */
typedef signed short        int16_t; /* 16-bit */
//typedef signed long         int32_t; /* 32-bit */
typedef signed long long    S6; /* 64-bit */

typedef struct{
   int16_t steer;
   int16_t speed;
   uint32_t crc;
} Serialcommand;
Serialcommand oCmd;

uint32_t crc32_for_byte(uint32_t r)
{
  for(int j = 0; j < 8; ++j)
  {
    r = (r & 1? 0: (uint32_t)0xEDB88320L) ^ r >> 1;
  }
  return r ^ (uint32_t)0xFF000000L;
}

void crc32(const void *data, size_t n_bytes, uint32_t* crc) {
  static uint32_t table[0x100] = {0};
  printf("!*table = %d\n",!*table);
  printf("n_bytes = %zu\n",n_bytes);
  if(!*table)
  {
    for(size_t i = 0; i < 0x100; ++i)
    {
      table[i] = crc32_for_byte(i);
      //printf("table in for = %lu\n",table[i]);
    }
  }
  for(size_t i = 0; i < n_bytes; ++i)
  {
    *crc = table[(uint8_t)*crc ^ ((uint8_t*)data)[i]] ^ *crc >> 8;
    printf("crc in for = %lu\n",crc[i]);
  }
}

void printdata(uint8_t* dataptr, size_t size)
{
  for (int i = 0; i < size; i++)
  {
    printf("dataptr : %d\n",dataptr[i]);
  }
}

int main ()
{
  #if 0
  unsigned int* int_pdata;
  unsigned int int_data;
  int_pdata = &int_data;
  int_data = 0x06;
  printf ("Address of int_pdata = %d\n",(unsigned int)int_pdata);
  printf ("Data in int_pdata = %d\n",*int_pdata);
  *int_pdata = (unsigned int)0x07;
  printf ("Data to int_pdata = %d\n",*int_pdata);
  printf ("Data in int_data = %d\n",int_data);

  unsigned int* int_pdata1;
  unsigned int* int_pdata2;
  //unsigned int data[6] = {0x50, 0x52, 0x49, 0x43, 0x4F, 0x4C};
  unsigned int data[MAX_LENGTH] = {50, 52, 49, 43, 41, 40};
  unsigned int data1[MAX_LENGTH] = {0};
  int_pdata1 = &data[0];
  int_pdata2 = &data1[0];


  //printf ("Address of int_pdata2 = %d\n",(unsigned int)int_pdata2);
  //printf ("Address of int_pdata1 = %d\n",(unsigned int)int_pdata1);
  //for (int instance = 0; instance < MAX_LENGTH; instance++)
  //{
  //  int_pdata2[instance] = int_pdata1[instance];
  //  printf ("Data in int_pdata1 = %d\t",int_pdata1[instance]);
  //  printf ("Data in int_pdata2 = %d\n",int_pdata2[instance]);
  //  printf ("Address of int_pdata1 = %d\t",(unsigned int)&int_pdata1[instance]);
  //  printf ("Address of int_pdata2 = %d\n",(unsigned int)&int_pdata2[instance]);
  //  printf ("Data in data = %d\t\t\t\t",data[instance]);
  //  printf ("Data in data1 = %d\n",data1[instance]);
  //}
int pattern_of_xs[5] = {5, 2, 5, 2, 2};
  for (int i = 0; i < 5; i++)
  {
    for (int j = 0; j < pattern_of_xs[i]; j++)
    {
      printf("x");
    }
    printf("\n");
  }
 #endif

  uint32_t crc = 0;
  oCmd.steer = 0;
  oCmd.speed = -102;
  //printf("crc32_for_byte : %lu\n",crc32_for_byte(0));
  crc32((const void *)&oCmd, sizeof(Serialcommand)-4,   &crc);
  oCmd.crc = crc;
  // 3826504196
  printf("Crc: %lu\n",oCmd.crc);
  unsigned char and_temp = 2;
  printf("and_temp : %d\n",(and_temp & 1));
  printf("Frequency : %lu\n",(uint32_t)1.1E7);
  //printdata((uint8_t *) &oCmd, sizeof(oCmd));

  return (0);
}
