/**
 * Comment Lines:
 * ----
 *
 * Comment lines are present in the program to access the code written by
 * the programmer. Later to development of the program itself some random
 * person who's reviewing the code would easy understand code's intention.
 * These comment lines doesn't affect the program in any way if properly
 * terminated, like this one.
 * In the below comments we'll be using single line comments for knowing
 * about the types of comments available in C++ language, the reason is that
 * the multi-line should not contain the end temination before the actual end
 * of comment. So only the single comment is suited to show you the list
 * below.
 *
 * Note:
 *  Some compilers might not support comments within comments will output a
 *  warning but if you're not bothered about that then you can leave it.
 */
 // There are two types of comment statements available
 // Single line comment: //comment here
 // Multi-line comment: /* */

/**
 * @brief      The preprocessing directive #ifndef will define the said #define
 *             here if it is not defined elsewhere. This is used to stop
 *             re-defining the same header file every time it's invoked or
 *             included in code.
 *
 * @return     None
 */
#ifndef __102_FUNCTIONS_H__
#define __102_FUNCTIONS_H__

/**
 * The functions that are present here can be use by other source files if
 * invoked properly.
 */
/**
 * Function Type: 5
 * ----
 *
 * The function decleration and function definition are in seperate files.
 */
/**
 * @brief      To divide the dividend by the divisor
 *
 * @param      int_dividend  The int dividend
 * @param      int_divisor   The int divisor
 *
 * @return     The actual divided value
 */
float functionsDivTwoInt (int int_dividend, int int_divisor);

/**
 * @brief      To find the tenth place of the integer
 *
 * @param      int_variable  The integer variable
 *
 * @return     Tenth place digit
 */
int funtionsFindTenthPlaceDigit (int int_variable);

/**
 * Function Type: 6
 * ----
 *
 * The function decleration and function definition are in the same location
 * within the file.
 * The inline keyword will define and declare the function in the same memory
 * unlike other type of declerations and definitions
 * In this type of function if the function doesn't contain static keyword along
 * with inline keyword as stated below it'll output a compiler error due to
 * compiler optimisation.
 *
 * This is one of the side effect of G++ the way it handle inline function.
 * When compiled, G++ performs inline substitution as the part of optimisation.
 * So there is no function call present (functionsMultiTwoInt) inside main. 
 * Please check below assembly code which compiler will generate. 
 * If you see there's not function call relating to functionsMultiTwoInt() 
 * in non-static version.
 *
 * Without static keyword:
 * ----
 * Error code:
 * Undefined symbols for architecture x86_64:
 * "_functionsMultiTwoInt", referenced from:
 *    _main in 102_functions-1190b1.o
 * ld: symbol(s) not found for architecture x86_64
 * error: linker command failed with exit code 1 (use -v to see invocation)
 *
 * With static keyword:
 * ----
 * refer : static.txt
 */
/**
 * @brief      To multiply two integers
 *
 * @param      int_multiplicant  The int multiplicant
 * @param      int_multiplier    The int multiplier
 *
 * @return     Multiplication result
 */
static inline int functionsMultiTwoInt (int int_multiplicant, int int_multiplier)
{
  /**
   * Returns the multiplication result
   */
  return (int_multiplicant * int_multiplier);
}

/**
 * Function Type: 7
 * ----
 *
 * The function pointer decleration.
 */
typedef int (*functionPointerIntType) (int);

#endif /* __102_FUNCTIONS_H__ */