"""
  Description:
  ----
    Python is a scripting language

  Functions:
  ----
    A function is a specific application which is designed to do just that alone
    Say, You want to write a code that will add two numbers and will display the
    result of the said addition of any two numbers given. Later, when moving on
    to learn more about coding if you are interested enough maybe you want to
    use the same code to add some numbers together you would have write the
    logic for the said addition again. Instead of doing that, what if there was
    a way to that without writing the same code again and again. That is were
    these functions come in help.

  Packages:
  ----
    Packages maybe defined as a group of these said functions which are directly
    associated with the building of the application.

  Projects:
  ----
    Projects are defined as the group of programs clustered together to form an
    definite application.

  Directories:
  ----
    Directories are necessarily folders containing either folders or files or
    both at the same time.

  File naming, structuring, etc:
  ----
    In Python the exists only a single type of file with the extension (.py).
    Depending on the usage it can be used as a source file or library or
    packages
    for programming. All the structuring, syntax-ing is/will be according to the
    PIP8 standards document.

    Link :
    ----
      https://www.python.org/dev/peps/pep-0008/

  Including Files/Modules:
  ----
    In order to include files to your own program as such given below the
    'import' is used with a file name.

    Example:
    ----
    # To import the C language operations onto python source
    import c_types

    (or)

    # To load the entire module onto the current source
    from c_types import *

  For More Information:
  ----
    Please refer the documents in the repository under the name
    Python_script_documentation.org

    Note:
    ----
      This document is an org file (.org) so you might need some special
      software to read the file properly. We do recommend the following
      software which works best in said configuration.
        1. Emacs under Spacemacs configuration Link: http://spacemacs.org/#
        2. GitLab repository viewer

    Editors & Integrated Development Environment(IDEs):
    ----

"""
"""
  Comment Lines:
  ----
    Comment lines are present in the program to assess the code written by
    the programmer. Later to development of the program itself some random
    person who's reviewing the code would easy understand code's intention.
    These comment lines doesn't affect the program in any way if properly
    terminated, like this one.
    In the below comments we'll be using single line comments for knowing
    about the types of comments available in C language, the reason is that
    the multi-line should not contain the end termination before the actual end
    of comment. So only the single comment is suited to show you the list
    below.

  Note:
  ----
    Some compilers might not support comments within comments, will output a
    warning but if you're not bothered about that then you can leave it.

  There are two types of comment statements available

  Single line comment:
  ----
    # comment here

  Multi-line comment:
  ----
    ''' comment here ''' (or) similar comment given in this message.
"""
condition_variable = True

"""
  Conditional Statements:
  ----
    The conditional statements are statements which execute when curtain
    specified conditions are met.
"""
if condition_variable:
  print(f'Is the Condition Good?\n{condition_variable}')
else:
  print(f'Is the Condition Bad?\n{condition_variable}')

if condition_variable == True:
  print(f'Condition is {condition_variable}')
else:
  print(f'Condition is {condition_variable}')

