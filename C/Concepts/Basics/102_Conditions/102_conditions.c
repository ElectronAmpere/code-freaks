/**
 * Description:
 * ----
 *
 * The #include <filename.extension> is a pre-processing directive which will
 * be added to program during compilation. Pre-processing is the process which
 * is done well before the code is processed by the target CPU on which it's
 * running on. All the pre-processing keywords will be represented by the
 * prefix '#' and the values assigned to said keyword will replace the keywords
 * presence in the program before the code is processed by the target CPU.
 * The #include will include the files that is necessary for application
 * program, in this case the basic input and output manipulation file stdio.h
 * which stands the standard input and output file.
 *
 * Functions:
 * ----
 *
 * A function is a specific application which is designed to do just that alone
 * Say, You want to write a code that will add two numbers and will display the
 * result of the said addition of any two numbers given. Later, when moving on
 * to learn more about coding if you are interested enough maybe you want to
 * use the same code to add some numbers together you would have write the
 * logic for the said addition again. Instead of doing that, what if there was
 * a way to that without writing the same code again and again. That is were
 * these functions come in help.
 *
 * Programs:
 * ----
 *
 * Programs maybe defined as a group of these said functions which are directly
 * associated with the building of the application.
 *
 * Projects:
 * ----
 *
 * Projects are defined as the group of programs clustered together to form an
 * definite application.
 *
 * Directories:
 * ----
 *
 * Directories are necessarily folders containing either folders or files or
 * both at the sametime.
 *
 * File naming, structuring, etc:
 * ----
 *
 * In the C language there exists different types of file depending on their
 * extension names.
 *
 * [Headerfile.h] --->
 * The header file which is given by .h will generally contain the function
 * names with inputs that can be used in other programs.
 *
 * [Sourcefile.c] --->
 * The source file which is given by .c will generally contain the function
 * processing based on the inputs given to the function.
 *
 * [Objectfile.o] --->
 * The object file which is given by .o will generally contain the machine
 * understandable code which is binary but some may output as hexadecimals, etc
 *
 * [Archivedfile.a] --->
 * The archieved file which is given by .a will contain the basic files needed
 * to program the application code which we will be writing below. The usage of
 * these files can be understood by reading any book related to C language but
 * the actual working of the functions within these files are not visible to
 * know about them.
 *
 * Including Files:
 * ----
 *
 * In order to include files to your own program as such given below the
 * #include is used with a file name in <> in case you are using the standard
 * library files. These library files are archived files are to be written
 * within <stdio.h>. In certain cases we'll be using "" to assign files to
 * programs which indicate that these files have visible source code within
 * the project directory.
 *
 *
 *
 * For More Information:
 * ----
 * Please refer the documents in the repository under the name
 * C_Language_documentation.org
 *
 * Note:
 *    This document is an org file (.org) so you might need some special
 *    softwares to read the file properly. We do recommand the following
 *    softwares which works best in said configuration.
 *      1. Emacs under Spacemacs configuration Link: http://spacemacs.org/#
 *      2. Sublime Text in org-mode Link: https://www.sublimetext.com
 *      3. GitLab repository viewer
 */

/**
 * Comment Lines:
 * ----
 *
 * Comment lines are present in the program to access the code written by
 * the programmer. Later to development of the program itself some random
 * person who's reviewing the code would easy understand code's intention.
 * These comment lines doesn't affect the program in any way if properly
 * terminated, like this one.
 * In the below comments we'll be using single line comments for knowing
 * about the types of comments available in C language, the reason is that
 * the multi-line should not contain the end temination before the actual end
 * of comment. So only the single comment is suited to show you the list
 * below.
 *
 * Note:
 *  Some compilers might not support comments within comments will output a
 *  warning but if you're not bothered about that then you can leave it.
 */
 // There are two types of comment statements available
 // Single line comment: //comment here
 // Multi-line comment: /* */
#include <stdio.h>

int main ()
{
  /**
   * Variable:
   * ----
   *
   * A variable is location to store information necessary to process in a
   * given program.
   * The variables declared in the function definition will only be accessable
   * only within that function. If accessed compilation errors will occur.
   */
  int int_variable1,int_variable2,int_functionresult;

  printf("Enter integer value1: ");
  scanf("%d",&int_variable1);
  printf("Enter integer value2: ");
  scanf("%d",&int_variable2);

  /**
   * If - else statement:
   * ----
   *
   * The if-else statement is pretty much self explanatory, there is an if
   * condition which must be held true in order to enter the statement or
   * else, the else statement.
   *
   * Syntax:
   * ----
   *
   * if-else:
   * -----
   *
   * if (relational condition only)
   * {
   *   statements;
   *   .
   *   .
   *   .
   * }
   * else
   * {
   *   statements;
   *   .
   *   .
   *   .
   * }
   *
   * if-else if-else;
   * ----
   *
   * if (relational condition only)
   * {
   *   statements;
   *   .
   *   .
   *   .
   * }
   * else if (relational condition only)
   * {
   *   statements;
   *   .
   *   .
   *   .
   * }
   * else
   * {
   *   statements;
   *   .
   *   .
   *   .
   * }
   *
   * Note:
   *  The if statement can be used alone but the else cannot exists alone
   *  without an if statement.
   *  The if statement can also be proceeded by an else if statement.
   *
   */
  /**
   * Here, the condition that's checked by the if statement is whether the
   * int_variable1 is greater than or equal to int_variable2 and in the else
   * statement will be implied that int_variable2 is greater than int_variable1
   * so the below statement make sense.
   */
  if (int_variable1 >= int_variable2)
  {
    int_functionresult = int_variable1 - int_variable2;
  }
  else
  {
    int_functionresult = int_variable2 - int_variable1;
  }

  printf("Result: %d\n",int_functionresult);

  printf("Enter dividend: ");
  scanf("%d",&int_variable1);
  printf("Enter divisor: ");
  scanf("%d",&int_variable2);

  /**
   * From the previous statements the below will be an example to if-else if
   * and so on ...
   * The below example may not make sense but guess what it's an example so
   * don't bother.
   */
  if (int_variable2 == 1)
  {
    printf("Result:%d\n",int_variable1);
  }
  else if (int_variable2 == 2)
  {
    printf("Result:%d\n",int_variable1/int_variable2);
  }
  else if (int_variable2 == 3)
  {
    printf("Result:%d\n",int_variable1/int_variable2);
  }
  else
  {
    printf("Result:%d\n",int_variable1/int_variable2);
  }

  printf("Enter a number: ");
  scanf("%d",&int_variable1);

  /**
   * The switch case statement:
   *
   * Syntax:
   * ----
   *
   * switch (expression)
   *  {
   *    case constant-expression:
   *
   *    { (optional-brackets)
   *
   *      statement1(s);
   *
   *    } (optional-brackets)
   *
   *    break; (optional)
   *
   *    case constant-expression:
   *
   *      statement2(s);
   *
   *    break; (optional)
   *    .
   *    .
   *    .
   *    (you can have any number of case statements)
   *
   *    default: (optional)
   *
   *      statementn(s);
   *
   *    break; (optional)
   *  }
   */
  switch (int_variable1)
  {
    case 0:
      printf(" 0\n ");
    break;
    case 1:
      printf(" 1\n ");
    break;
    case 2:
      printf(" 2\n ");
    break;
    case 3:
      printf(" 3\n ");
    break;
    case 4:
      printf(" 4\n ");
    break;
    default:
      printf(" default\n ");
    break;
  }

  /**
   * To hold program until result is visualised
   */
  scanf("%d",&int_variable1);

  /**
   * The switch case
   */
  switch (int_variable1)
  {
    case 0:
    {
      printf(" 0\t ");
      switch (int_variable2)
      {
        case 0:
          printf(" 0\n ");
        break;
        case 1:
          printf(" 1\n ");
        break;
        default:
          printf(" default\n ");
        break;
      }
    }
    break;
    case 1:
      printf(" 1\t ");
    break;
    case 2:
      printf(" 2\t ");
    break;
    case 3:
      printf(" 3\t ");
    break;
    case 4:
      printf(" 4\t ");
    break;
    default:
      printf(" default\n ");
    break;
  }

  /**
   * To hold program until result is visualised
   */
  scanf("%d",&int_variable1);

  /**
   * The below statement has a single-line condition statement
   *
   * Syntax:
   * ----
   *
   * Terinary Conditional Statement:
   * ----
   *
   * (relational condition only)?statement1:statement2;
   *
   * (check relational condition only)?if true execute this:else execute this;
   *
   * Note:
   * ----
   *
   * This can also be nested like the others
   *
   * Example:
   * ----
   * (if condition1 true)?(if condition2 is true)?if condition2 true execute this :else execute this :else if condition1 false execute this;
   *  |                                |             ^                  ^                 ^                            ^
   *  |    if true will come here      |             |                  |                 |                            |
   *  ---------------------------------|--------------                  |                 |                            |
   *  |                                |                                |                 |                            |
   *  |                                |        if false will come here |                 |                            |
   *  ---------------------------------|--------------------------------|-----------------|-----------------------------
   *                                   |    if true will come here      |                 |
   *                                   ----------------------------------                 |
   *                                   |                                                  |
   *                                   |            if false will come here               |
   *                                   ----------------------------------------------------
   *
   */
  printf("%d",(int_variable1 > 0)?int_variable1:0xFF);

  /**
   * This is the non-essential return statement for the function when specified
   * the return has to be 0 if not then you just don't care.
   */
  return (0);
}

/**
 * Links:
 *  https://www.dotnettricks.com/learn/c/conditional-statements-if-else-switch-ladder
 */
