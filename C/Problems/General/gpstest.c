#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "minmea.h"
#define INDENT_SPACES "  "

int main(void)
{
    char line[MINMEA_MAX_LENGTH] = "$GNRMC,105036.00,A,1101.24761,N,07656.38644,E,0.056,,230719,,,A,V*10";
    printf("%s", line);
    if (minmea_sentence_id(line, false) == MINMEA_SENTENCE_RMC)
    {
      struct minmea_sentence_rmc frame;
      if (minmea_parse_rmc(&frame, line)) {
          printf("Lat:%f ,Lon:%f\n",minmea_tocoord(&frame.latitude),minmea_tocoord(&frame.longitude));
      }
      else {
          printf(INDENT_SPACES "$xxRMC sentence is not parsed\n");
      }
    }
    return 0;
}