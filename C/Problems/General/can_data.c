#include <stdio.h>

#define DATA_LENGTH_8b        (128U)
#define DATA_LENGTH_64b       (16U)
#define DATA_LENGTH_32b       (32U)
#define DATA_LENGTH_BYTECOUNT (8U)

#define BITS_FOR_BYTE         (8U)
#define BITS_FOR_DWORD        (64U)

#define DATA_CANBUS_LENGTH    (2U)

typedef unsigned char       uint8_t;
typedef unsigned short      uint16_t;
typedef unsigned long       uint32_t;
typedef unsigned long long  uint64_t;


int main()
{
  /**
   * Data:
   * ----
   * #VNAME1 $VNUM6384 *64533 +87432 A7654 B6364 C6473 D6473 E9182 F7464 G6748 H2347 I9847 J1238
   *
   * Parsed Data:
   * ----
   * 23564e414d453120 24564e554d363338 34202a3634353333
   * 202b383734333220 4137363534204236 3336342043363437
   * 3320443634373320 4539313832204637 3436342047363734
   * 3820483233343720 4939383437204a31 3233380000000000
   */
  uint8_t  updatecandata[DATA_LENGTH_8b] = "#VNAME134 $VNUM6384 *64533 +87432 A7654 B6364 C6473 D6473 E9182 F7464 G6748 H2347 I9847 J1238";
  uint8_t  localdata[DATA_LENGTH_64b][DATA_LENGTH_BYTECOUNT] = {0};
  uint64_t datasingle_64b = 0;
  uint64_t dataarray_64b[DATA_LENGTH_64b] = {0};
  uint32_t canbusdata[DATA_LENGTH_64b][DATA_CANBUS_LENGTH] = {0};

  /**
   * This for loop is used to transmitting data to can bus
   */
  for (uint8_t row_BYTES = 0; row_BYTES < DATA_LENGTH_64b; row_BYTES++)
  {
    for (uint8_t column_BYTE = 0; column_BYTE < DATA_LENGTH_BYTECOUNT ; column_BYTE++)
    {
      localdata[row_BYTES][column_BYTE] = updatecandata[column_BYTE + (BITS_FOR_BYTE * row_BYTES)];
    }
    printf("localdata[%d] = %s\t",row_BYTES,localdata[row_BYTES]);
    printf("|\t0x%s\n",localdata[row_BYTES]);
  }
#if 0
  datasingle_64b = (uint64_t)((uint64_t)localdata[0][0] << 56 | (uint64_t)localdata[0][1] << 48 |
                              (uint64_t)localdata[0][2] << 40 | (uint64_t)localdata[0][3] << 32 |
                              (uint64_t)localdata[0][4] << 24 | (uint64_t)localdata[0][5] << 16 |
                              (uint64_t)localdata[0][6] << 8  | (uint64_t)localdata[0][7]);
  printf("Complete datasingle_64b = %llx\n",datasingle_64b);
#endif
  /**
   * Can bus data conversion
   */
  for (uint8_t row_DWORD = 0; row_DWORD < DATA_LENGTH_64b; row_DWORD++)
  {
    for (uint8_t column_WORD = 0; column_WORD < DATA_CANBUS_LENGTH; column_WORD++)
    {
      for (uint8_t column_BYTE = 0; column_BYTE < DATA_LENGTH_BYTECOUNT/2; column_BYTE++)
      {
        canbusdata[row_DWORD][column_WORD] |= localdata[row_DWORD][(column_WORD > 0)?column_BYTE+4:column_BYTE] << (BITS_FOR_BYTE * column_BYTE);
      }
      printf("for canbusdata[%d][%d] = %lu\t",row_DWORD,column_WORD,canbusdata[row_DWORD][column_WORD]);
      printf("| 0x%lx\n",canbusdata[row_DWORD][column_WORD]);
    }
  }

#if 0
  /**
   * This for loop is used to receiving data from can bus
   */
  for (uint8_t row_DWORD = 0; row_DWORD < DATA_LENGTH_64b; row_DWORD++)
  {
    datasingle_64b = (uint64_t)0;
    for(uint8_t column_BYTE = 0; column_BYTE < DATA_LENGTH_BYTECOUNT; column_BYTE++)
    {
      datasingle_64b           |= (uint64_t)((uint64_t)localdata[row_DWORD][column_BYTE] << (BITS_FOR_DWORD - BITS_FOR_BYTE * (column_BYTE + 1)));
      dataarray_64b[row_DWORD] |= (uint64_t)((uint64_t)localdata[row_DWORD][column_BYTE] << (BITS_FOR_DWORD - BITS_FOR_BYTE * (column_BYTE + 1)));
      printf("for datasingle_64b = %llx\n",datasingle_64b);
    }
    printf("for dataarray_64b[%d] = %llx\n",row_DWORD,dataarray_64b[row_DWORD]);
  }
#endif

  /**
   * This for loop is used to receiving data from can bus
   */
  for (uint8_t row_DWORD = 0; row_DWORD < DATA_LENGTH_64b; row_DWORD++)
  {
    for(uint8_t column_WORD = DATA_CANBUS_LENGTH; column_WORD > 0; column_WORD--)
    {
      dataarray_64b[row_DWORD] |= (uint64_t)((uint64_t)canbusdata[row_DWORD][column_WORD-1] << 32 * (column_WORD-1));
    }
    printf("for dataarray_64b[%d] = %llx\n",row_DWORD,dataarray_64b[row_DWORD]);
  }
  printf("dataarray_64b = %s\n",(uint8_t*)dataarray_64b);

  float adc1  = 1.34,
        adc2  = 2.45,
        adc3  = 3.65,
        adc4  = 1.23,
        adc5  = 5.66,
        adc6  = 4.33,
        adc7  = 3.12,
        adc8  = 2.31,
        adc9  = 4.11,
        adc10 = 5.88,
        pay  = 10.365,
        gvw  = 112.733;
  uint8_t fa1,fa2,la,ra1,ra2,upay,ugvw[2];
  fa1 = (uint8_t)(int)(adc1*10+adc2*10);
  fa2 = (uint8_t)(int)(adc3*10+adc4*10);
  la  = (uint8_t)(int)(adc5*10+adc6*10);
  ra1 = (uint8_t)(int)(adc7*10+adc8*10);
  ra2 = (uint8_t)(int)(adc9*10+adc10*10);
  upay = (uint8_t)(int)(pay*10);
  ugvw[0] = (uint8_t)(int)(gvw*10);
  ugvw[1] = (uint8_t)((int)gvw*10 >> 8);

  printf("fa1 = %hhu\n",fa1);
  printf("fa2 = %hhu\n",fa2);
  printf("la = %hhu\n",la);
  printf("ra1 = %hhu\n",ra1);
  printf("ra2 = %hhu\n",ra2);
  printf("upay = %hhu\n",upay);
  printf("ugvw[0] = %hhu\t",ugvw[0]);
  printf("| 0x%hhx\n",ugvw[0]);
  printf("ugvw[1] = %hhu\t",ugvw[1]);
  printf("| 0x%hhx\n",ugvw[1]);

  uint8_t u8dataarray[8] = {0x43,0x34,0x32,0x45,0x40,0x39,0x37,0x43};
  uint8_t* u8pointer;
  u8pointer = u8dataarray;
  uint32_t* u32pointer = (uint32_t*)u8pointer;
  uint32_t u32data[2] = {0};
  u32data[0] = *u32pointer;
  u32data[1] = *(u32pointer+1U);
  printf("u32data[0] = 0x%lx\n",u32data[0]);
  printf("u32data[1] = 0x%lx\n",u32data[1]);
  return 0;
}
