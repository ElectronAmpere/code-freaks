/**
 * Comment Lines:
 * ----
 *
 * Comment lines are present in the program to assess the code written by
 * the programmer. Later to development of the program itself some random
 * person who's reviewing the code would easy understand code's intention.
 * These comment lines doesn't affect the program in any way if properly
 * terminated, like this one.
 * In the below comments we'll be using single line comments for knowing
 * about the types of comments available in C++ language, the reason is that
 * the multi-line should not contain the end temination before the actual end
 * of comment. So only the single comment is suited to show you the list
 * below.
 *
 * Note:
 *  Some compilers might not support comments within comments will output a
 *  warning but if you're not bothered about that then you can leave it.
 */
 // There are two types of comment statements available
 // Single line comment: //comment here
 // Multi-line comment: /* */
#include <iostream>


//#define NAMESPACE_ENABLED
/**
 * Why “using namespace std” is considered bad practice?
 *
 * The statement using namespace std is generally considered bad practice.
 * The alternative to this statement is to specify the namespace to which
 * the identifier belongs using the scope operator(::) each time we declare a type.
 *
 * Although the statement saves us from typing std:: whenever we wish to
 * access a class or type defined in the std namespace, it imports the
 * entirety of the std namespace into the current namespace of the program.
 *
 * Namespaces were introduced into C++ to resolve identifier name conflicts.
 * This ensured that two objects can have the same name and yet be treated
 * differently if they belonged to different namespaces. Notice how the exact
 * opposite has occurred in this example. Instead of resolving a name conflict,
 * we actually create a naming conflict.
 *
 * When we import a namespace we are essentially pulling all type definitions
 * into the current scope. The std namespace is huge. It has hundreds of predefined
 * identifiers, so it is possible that a developer may overlook the fact there is
 * another definition of their intended object in the std library.
 * Unaware of this they may proceed to specify their own implementation and expect
 * it to be used in later parts of the program. Thus there would exist two definitions
 * for the same type in the current namespace. This is not allowed in C++, and even
 * if the program compiles there is no way of knowing which definition is being used where.
 * The solution to the problem is to explicitly specify to which namespace our identifier
 * belongs to using the scope operator (::).
 */
#ifdef NAMESPACE_ENABLED
    using namespace std;
#endif

/**
 * Function Definition Syntax:
 * ----
 *
 * The basic syntax of any function that's written or yet to be is given by
 * return_datatype function_name (datatype1 argument1,datatype2 argument2,datatype3 argument3,...)
 * { start_of_function
 *    //comments
 *    statements are written here; statement_termiation
 *    returns;
 * } end_of_function
 *
 * If your wondering what does this all mean see the code below you might
 * understand.
 */
/**
 * @brief      This is called the main function of the program. It is assumed
 *             that the program starts here generally. But that is not the case
 *             in every system, in embedded system the actual code starts some
 *             other place than the main function which is congfigured in the
 *             file starup file startup.s which we'll be analysing later.
 *             The code below is called a function it has arguments as void and
 *             returns an integer.
 *
 *             The function main is said to display the information/message
 *             "Your First program" when executed/run
 *
 * @fn         int main(void)
 *
 * @param      void  The void
 *
 * @return     Returns the value zero at the end of the function execution
 *             Apparently, any value other than zero results in a error code
 *             which will also dealt with later.
 *
 * @error     error: 'main' must return 'int'
 *                void main(void)
 *            If the int main (void) or int main () is replaced with
 *            void main (void) or any other datatype is replies with an error
 *            like the above error:  statement.
 */
int main(void)
{
    /**
     * This statement is a function call to the standard message
     * display function in the standard library under the name
     * cout<<"Message";
     * 
     * Unlike C the cout doesn't need the format specifier like
     * %d, %c, %f, %s ...
     */
    std::cout<<"Hi, First C++\n";

    std::cout<<"Here's a number "<<1<<"\n";
    std::cout<<"Length of Hello string: \n"<<sizeof("Hello");
    std::cout<<"Length of Hello string: \n"<<(long)"Hello";
    std::cout<<"\n";

    /**
     * This is the non-essential return statement for the function when specified
     * the return has to be 0 if not then you just don't care enough.
     */
    return (0);
}