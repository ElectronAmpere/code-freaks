/**
 * @defgroup   1DARRAYINC 1 D Arrayin C
 *
 * @brief      This file implements 1 D Arrayin C.
 *
 * @author     Vigneshrajarathinam
 * @date       2020
 */

/**
 * An array is a container object that holds a fixed number of values of a single type. To create an array in C, we can do int arr[n];. Here, arr, is a variable array which holds up to  integers. The above array is a static array that has memory allocated at compile time. A dynamic array can be created in C, using the malloc function and the memory is allocated on the heap at runtime. To create an integer array,  of size , int *arr = (int*)malloc(n * sizeof(int)), where  points to the base address of the array.
In this challenge, you have to create an array of size  dynamically, input the elements of the array, sum them and print the sum of the elements in a new line.
Input Format
The first line contains an integer, .
The next line contains  space-separated integers.
Constraints


Output Format
Print in a single line the sum of the integers in the array.
Sample Input 0
6
16 13 7 2 1 12
Sample Output 0
51
Sample Input 1
7
1 13 15 20 12 13 2
Sample Output 1
76
 */
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main() {

    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int integer_count = 0, integer_sum = 0;
    scanf("%d\n",&integer_count);
    int *integer_array = (int*)malloc(integer_count * sizeof(int));
    for (int instance = 0; instance < integer_count; instance++)
    {
        scanf("%d ",(integer_array+instance));
        integer_sum += *(integer_array+instance);
    }
    printf("%d",integer_sum);
    return 0;
}