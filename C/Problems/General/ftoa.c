#include <stdio.h>
#include <string.h>

typedef unsigned char  uint8_t;
typedef unsigned short uint16_t;
typedef unsigned long  uint32_t;

static inline uint16_t pericommon_StringLength(char *c)
{
  uint16_t i;
  for (i = 0; c[i] != '\0'; i++);
  return i;
}

void reverse(char s[])
{
  int c, i, j;

  for (i = 0, j = pericommon_StringLength(s)-1; i<j; i++, j--) {
    c = s[i];
    s[i] = s[j];
    s[j] = c;
  }
}

void u_itoa( int n, char s[] )
{
  int i, sign;
  if(( sign = n ) < 0 )
    n = -n;

  i = 0;
  do
  {
    s[i++] = (n % 10) + '0';
  } while(( n /= 10 ) > 0 );

  if( sign < 0 )
    s[i++] = '-';

  s[i] = '\0';
  reverse( s );
}

void FloatToStringNew(char *str, double f, uint8_t size)
{
  uint8_t pos; // position in string
  uint8_t len; // length of decimal part of result
  char curr[10]; // temp holder for next digit
  uint32_t value; // decimal digit(s) to convert
  pos = 0; // initialize pos, just to be sure

  value = (uint32_t)f; // truncate the floating point number
  u_itoa(value,str); // this is kinda dangerous depending on the length of str
  // now str array has the digits before the decimal

  if (f < 0 ) // handle negative numbers
  {
    f *= -1;
    value *= -1;
  }

  len = pericommon_StringLength(str); // find out how big the integer part was
  pos = len; // position the pointer to the end of the integer part
  str[pos++] = '.'; // add decimal point to string

  while(pos < (size + len + 1) ) // process remaining digits
  {
    f = f - (double)value; // hack off the whole part of the number
    f *= 10; // move next digit over
    value = (uint8_t)f; // get next digit
    u_itoa(value, curr); // convert digit to string
    str[pos++] = *curr; // add digit to result string and increment pointer
  }
  str[pos++] = '\0';
}

int main()
{
  double ft = -1192.388376;
  char cft[30];
  printf("ft  = %.6f\n",ft);
  FloatToStringNew(cft,ft,6);
  printf("cft = %s\n",cft);
  return 0;
}
