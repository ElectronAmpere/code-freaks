/**
 * Description:
 * ----
 *
 * The #include <filename.extension> is a pre-processing directive which will
 * be added to program during compilation. Pre-processing is the process which
 * is done well before the code is processed by the target CPU on which it's
 * running on. All the pre-processing keywords will be represented by the
 * prefix '#' and the values assigned to said keyword will replace the keywords
 * presence in the program before the code is processed by the target CPU.
 * The #include will include the files that is necessary for application
 * program, in this case the basic input and output manipulation file stdio.h
 * which stands the standard input and output file.
 *
 * Functions:
 * ----
 *
 * A function is a specific application which is designed to do just that alone
 * Say, You want to write a code that will add two numbers and will display the
 * result of the said addition of any two numbers given. Later, when moving on
 * to learn more about coding if you are interested enough maybe you want to
 * use the same code to add some numbers together you would have write the
 * logic for the said addition again. Instead of doing that, what if there was
 * a way to that without writing the same code again and again. That is were
 * these functions come in help.
 *
 * Programs:
 * ----
 *
 * Programs maybe defined as a group of these said functions which are directly
 * associated with the building of the application.
 *
 * Projects:
 * ----
 *
 * Projects are defined as the group of programs clustered together to form an
 * definite application.
 *
 * Directories:
 * ----
 *
 * Directories are necessarily folders containing either folders or files or
 * both at the sametime.
 *
 * File naming, structuring, etc:
 * ----
 *
 * In the C language there exists different types of file depending on their
 * extension names.
 *
 * [Headerfile.h] --->
 * The header file which is given by .h will generally contain the function
 * names with inputs that can be used in other programs.
 *
 * [Sourcefile.c] --->
 * The source file which is given by .c will generally contain the function
 * processing based on the inputs given to the function.
 *
 * [Objectfile.o] --->
 * The object file which is given by .o will generally contain the machine
 * understandable code which is binary but some may output as hexadecimals, etc
 *
 * [Archivedfile.a] --->
 * The archieved file which is given by .a will contain the basic files needed
 * to program the application code which we will be writing below. The usage of
 * these files can be understood by reading any book related to C language but
 * the actual working of the functions within these files are not visible to
 * know about them.
 *
 * Including Files:
 * ----
 *
 * In order to include files to your own program as such given below the
 * #include is used with a file name in <> in case you are using the standard
 * library files. These library files are archived files are to be written
 * within <stdio.h>. In certain cases we'll be using "" to assign files to
 * programs which indicate that these files have visible source code within
 * the project directory.
 *
 *
 *
 * For More Information:
 * ----
 * Please refer the documents in the repository under the name
 * C_Language_documentation.org
 *
 * Note:
 *    This document is an org file (.org) so you might need some special
 *    softwares to read the file properly. We do recommand the following
 *    softwares which works best in said configuration.
 *      1. Emacs under Spacemacs configuration Link: http://spacemacs.org/#
 *      2. Sublime Text in org-mode Link: https://www.sublimetext.com
 *      3. GitLab repository viewer
 */

/**
 * Comment Lines:
 * ----
 *
 * Comment lines are present in the program to access the code written by
 * the programmer. Later to development of the program itself some random
 * person who's reviewing the code would easy understand code's intention.
 * These comment lines doesn't affect the program in any way if properly
 * terminated, like this one.
 * In the below comments we'll be using single line comments for knowing
 * about the types of comments available in C language, the reason is that
 * the multi-line should not contain the end temination before the actual end
 * of comment. So only the single comment is suited to show you the list
 * below.
 *
 * Note:
 *  Some compilers might not support comments within comments will output a
 *  warning but if you're not bothered about that then you can leave it.
 */
 // There are two types of comment statements available
 // Single line comment: //comment here
 // Multi-line comment: /* */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//void UpdateString (char* destination_string, char* source_string);
/**
 * Function Definition Syntax:
 * ----
 *
 * The basic syntax of any function that's written or yet to be is given by
 * return_datatype function_name (datatype1 argument1,datatype2 argument2,datatype3 argument3,...)
 * { start_of_function
 *    //comments
 *    statements are written here; statement_termiation
 *    returns;
 * } end_of_function
 *
 * If your wondering what does this all mean see the code below you might
 * understand.
 */
/**
 * @brief      This is called the main function of the program. It is assumed
 *             that the program starts here generally. But that is not the case
 *             in every system, in embedded system the actual code starts some
 *             other place than the main function which is congfigured in the
 *             file starup file startup.s which we'll be analysing later.
 *             The code below is called a function it has arguments as void and
 *             returns an integer.
 *
 *             The function main is said to display the information/message
 *             "Your First program" when executed/run
 *
 * @fn         int main(void)
 *
 * @param      void  The void
 *
 * @return     Returns the value zero at the end of the function execution
 *             Apparently, any value other than zero results in a error code
 *             which will also dealt with later.
 *
 * @error     error: 'main' must return 'int'
 *                void main(void)
 *            If the int main (void) or int main () is replaced with
 *            void main (void) or any other datatype is replies with an error
 *            like the above error:  statement.
 */
int main(void)
{
    char char_array[10] = "Helpeid87\0";
    char char_character = 'A';
    char *char_pointer;
    char char_2d_array[4][30]= {"woeineed","wedoiewdewde"};
    char *char_2d_pointer[4];
    *(char_2d_pointer+0) = char_2d_array[0];
    *(char_2d_pointer+1) = char_2d_array[1];
    char_pointer = &char_character;
    void UpdateString (char* destination_string, char* source_string);
    void PrintPointer (char* pointer, int length);
  	/**
   	 * The pointer will point to an address in the memory be it char, int, etc.
  	 * Say, if the pointer is pointing in memory of an char, which has only on character
  	 * in it's memory. The pointer will return the address that it points to by invoking
  	 * the pointer variable here 'char_pointer' (which must be typecasted).
   	 */
    printf("int of *char_pointer = %d\n",*char_pointer);
    printf("char of *char_pointer = %c\n",*char_pointer);
    printf("string of char_pointer = %s\n",char_pointer);
    printf("address of (int)char_pointer = %d\n",(int)char_pointer);
    printf("address of (int)&char_character = %d\n",(int)&char_character);

    /**
     * Incrementing a pointer:
     * We know that *char_pointer points to address of a character.
     * If we write in any of the following ways
     * 
     * char_pointer + 1, 
     * char_pointer += 1,
     * ++*char_pointer,
     * (*char_pointer)++ 
     * 
     * will increment the address of the pointer by one.
     */
    printf("char of *char_pointer+1 = %c\n",*char_pointer+1);


    printf("string of char_2d_pointer[0] = %s\n",char_2d_pointer[0]);
    printf("string in (*char_2d_pointer+0) = %s\n",(*char_2d_pointer+0));
    printf("string in *(char_2d_pointer+0) = %s\n",*(char_2d_pointer+0));

    printf("character in char_2d_pointer[0] = %c\n",char_2d_pointer[0][0]);
    printf("character in (*char_2d_pointer+0) = %c\n",*(*char_2d_pointer+0)+0);
    printf("character in *(char_2d_pointer+0) = %c\n",*(*(char_2d_pointer+0))+0);
    
    printf("string of char_2d_pointer[1] = %s\n",char_2d_pointer[1]);
    printf("string in (*char_2d_pointer+1) = %s\n",(*char_2d_pointer+1));
    printf("string in *(char_2d_pointer+1) = %s\n",*(char_2d_pointer+1));

    printf("character in char_2d_pointer[1] = %c\n",char_2d_pointer[1][0]);
    printf("character in (*char_2d_pointer+1) = %c\n",*(*char_2d_pointer+1)+0);
    printf("character in *(char_2d_pointer+1) = %c\n",*(*(char_2d_pointer+1))+0);

    printf("strlen(char_pointer) = %lu\n",strlen(char_pointer));
    printf("sizeof(char_pointer) = %lu\n",sizeof(char_pointer));
    printf("sizeof(char) = %lu\n",sizeof(char));

    PrintPointer(char_pointer, sizeof(char_pointer));
    #if 0
    printf("%s\n",char_array);
    UpdateString(char_array,"Cyans7890");
    printf("%s\n",char_array);
    #endif
    return (0);
}

/**
 * @brief      { function_description }
 *
 * @param      destination_string  The destination string
 * @param      source_string       The source string
 *
 * @return     { description_of_the_return_value }
 */
void UpdateString (char* destination_string, char* source_string)
{
    for (int i = 0; i < strlen(destination_string); i++)
       { destination_string[i] = source_string[i]; }
}

void PrintPointer (char* pointer, int length)
{
	int count = 0;
	while(count < length)
	{
		printf("%c",*(pointer+count));
		count++;
	}
}