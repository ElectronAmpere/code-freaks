/**
 * Description:
 * ----
 *
 * The #include <filename.extension> is a pre-processing directive which will
 * be added to program during compilation. Pre-processing is the process which
 * is done well before the code is processed by the target CPU on which it's
 * running on. All the pre-processing keywords will be represented by the
 * prefix '#' and the values assigned to said keyword will replace the keywords
 * presence in the program before the code is processed by the target CPU.
 * The #include will include the files that is necessary for application
 * program, in this case the basic input and output manipulation file stdio.h
 * which stands the standard input and output file.
 *
 * Functions:
 * ----
 *
 * A function is a specific application which is designed to do just that alone
 * Say, You want to write a code that will add two numbers and will display the
 * result of the said addition of any two numbers given. Later, when moving on
 * to learn more about coding if you are interested enough maybe you want to
 * use the same code to add some numbers together you would have write the
 * logic for the said addition again. Instead of doing that, what if there was
 * a way to that without writing the same code again and again. That is were
 * these functions come in help.
 *
 * Programs:
 * ----
 *
 * Programs maybe defined as a group of these said functions which are directly
 * associated with the building of the application.
 *
 * Projects:
 * ----
 *
 * Projects are defined as the group of programs clustered together to form an
 * definite application.
 *
 * Directories:
 * ----
 *
 * Directories are necessarily folders containing either folders or files or
 * both at the sametime.
 *
 * File naming, structuring, etc:
 * ----
 *
 * In the C language there exists different types of file depending on their
 * extension names.
 *
 * [Headerfile.h] --->
 * The header file which is given by .h will generally contain the function
 * names with inputs that can be used in other programs.
 *
 * [Sourcefile.c] --->
 * The source file which is given by .c will generally contain the function
 * processing based on the inputs given to the function.
 *
 * [Objectfile.o] --->
 * The object file which is given by .o will generally contain the machine
 * understandable code which is binary but some may output as hexadecimals, etc
 *
 * [Archivedfile.a] --->
 * The archieved file which is given by .a will contain the basic files needed
 * to program the application code which we will be writing below. The usage of
 * these files can be understood by reading any book related to C language but
 * the actual working of the functions within these files are not visible to
 * know about them.
 *
 * Including Files:
 * ----
 *
 * In order to include files to your own program as such given below the
 * #include is used with a file name in <> in case you are using the standard
 * library files. These library files are archived files are to be written
 * within <stdio.h>. In certain cases we'll be using "" to assign files to
 * programs which indicate that these files have visible source code within
 * the project directory.
 *
 *
 *
 * For More Information:
 * ----
 * Please refer the documents in the repository under the name
 * C_Language_documentation.org
 *
 * Note:
 *    This document is an org file (.org) so you might need some special
 *    softwares to read the file properly. We do recommand the following
 *    softwares which works best in said configuration.
 *      1. Emacs under Spacemacs configuration Link: http://spacemacs.org/#
 *      2. Sublime Text in org-mode Link: https://www.sublimetext.com
 *      3. GitLab repository viewer
 */

/**
 * Comment Lines:
 * ----
 *
 * Comment lines are present in the program to access the code written by
 * the programmer. Later to development of the program itself some random
 * person who's reviewing the code would easy understand code's intention.
 * These comment lines doesn't affect the program in any way if properly
 * terminated, like this one.
 * In the below comments we'll be using single line comments for knowing
 * about the types of comments available in C language, the reason is that
 * the multi-line should not contain the end temination before the actual end
 * of comment. So only the single comment is suited to show you the list
 * below.
 *
 * Note:
 *  Some compilers might not support comments within comments will output a
 *  warning but if you're not bothered about that then you can leave it.
 */
 // There are two types of comment statements available
 // Single line comment: //comment here
 // Multi-line comment: /* */
#include <stdio.h>

/* Defines */
#define MAXLINES 	(10000) /* Maximum input lines */

/* Function Prototypes */
int  getLine (char line[], int maxlength);
void copy (char to[], char from[]);

/**
 * Function Definition Syntax:
 * ----
 *
 * The basic syntax of any function that's written or yet to be is given by
 * return_datatype function_name (datatype1 argument1,datatype2 argument2,
 *                                datatype3 argument3,...)
 * { start_of_function
 *    //comments
 *    statements are written here; statement_termiation
 *    returns;
 * } end_of_function
 *
 * If your wondering what does this all mean see the code below you might
 * understand.
 */
/**
 * @brief      This is called the main function of the program. It is assumed
 *             that the program starts here generally. But that is not the case
 *             in every system, in embedded system the actual code starts some
 *             other place than the main function which is congfigured in the
 *             file starup file startup.s which we'll be analysing later.
 *             The code below is called a function it has arguments as void and
 *             returns an integer.
 *
 *             The function main is said to display the information/message
 *             "Your First program" when executed/run
 *
 * @fn         int main(void)
 *
 * @param      void  The void
 *
 * @return     Returns the value zero at the end of the function execution
 *             Apparently, any value other than zero results in a error code
 *             which will also dealt with later.
 */
int main(void)
{
	/**
	 * Variable initialisations and primary assignments
	 */
	char lines[MAXLINES] = "\0", longest[MAXLINES] = "\0";
	int max = 0, len = 0;
	
	printf("Prints the longest line entered\nMaximum lines allowed is %d\n",MAXLINES);
	printf("To view result press CTRL+D\nNow, Start Typing ...\n");

	/**
	 * Here when the length of the line entered is zero
	 * then the validation of the loop fails and exits.
	 * Even if the brackets {} doesn't enclose the loop
	 * it'll not fail. Beacause the getchar() inside of
	 * the getLine() will temporarily hold up the execution
	 * until any input is entered. Hence the while loop here
	 * seems to defy common logic.
	 */
	while((len = getLine(lines,MAXLINES)) > 0)
	//{
		/**
		 * If len is greater than max replace max with len value.
		 */
		if (len > max)
		{
			max = len;
			/**
			 * Copying the contents of the lines string the longest.
			 */
			copy(longest, lines);
		}
	//}
	if (max > 0)
		printf("Longest: %s",longest);
	return (0);
}

/* Function Definitions */
/**
 * @brief      Reads the line into the line and returns it's length
 *
 * @param      line           The line
 * @param[in]  maxlength  The maxlength
 *
 * @return     line length
 */
int getLine (char line[], int maxlength)
{
	int int_of_char, instance;
	/**
	 * The loop conditions are as follows
	 * - the instance value or the array index is compared with
	 * 	 the maximum length of the maxlength variable.
	 * - the int_of_char value is conpared to the ASCII value of the
	 *   getchar() function and to the End Of File (EOF) value.
	 * - then int_of_char is compared to the new-line '\n' value.
	 */
	/**
	 * Note:
	 *  	The int_of_char is a integer variable but the value form the 
	 *  	getchar() represents a character in integer form. The integer
	 *  	is then stored on to a string varibale (container). This 
	 *  	assignment resolves any issues that may arise with integer 
	 *  	to character conversion.
	 */
	for(instance = 0; (instance < maxlength - 1) 
				   && ((int_of_char = getchar())!= EOF)
				   && (int_of_char != '\n');++instance)
	{
		//printf("instance = %d\n",instance);
		line[instance] = int_of_char;
	}

	/**
	 * The new line is also considered as a character along the entered
	 * line.
	 */
	if (int_of_char == '\n')
	{
		line[instance] = int_of_char;
		++instance;
	}

	/**
	 * Appends the Null character '\0' at the end of the line.
	 * Signification of string end.
	 */
	line[instance] = '\0';

	/**
	 * Returns the length of the string.
	 */
	return (instance);
}

/**
 * @brief      copy: copy 'from' into 'to'; assume to is big enough
 *
 * @param      to    The destination to copy to
 * @param      from  The source to copy from
 */
void copy (char to[], char from[])
{
	int instance = 0;
	/**
	 * Assignes the value from to to character by character and also
	 * compares the resulting character with Null '\0' to signify
	 * end.
	 */
	while ((to[instance] = from[instance]) != '\0')
		++instance;
}