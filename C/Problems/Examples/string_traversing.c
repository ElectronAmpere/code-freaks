#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define SUBSTRINGS_MAX 		(6)
typedef struct {
	char *pNum[SUBSTRINGS_MAX];
}info;

int checkCharacterInString(const char character, const char *string)
{
	while(*string != '\0')
	{
		if (character == *string)
		{
			return (1);
		}
		string++;
	}
	return (0);
}
void getSubString(char **pdestination, const char *psource, const char *delimiter)
{
	int substrings = 0, destinationcol = 0;
	while (*psource != '\0')
	{
		//if((*psource == *(delimiter+0))||(*psource == *(delimiter+1)))
		if(checkCharacterInString(*psource,delimiter))
		{
			pdestination[substrings][destinationcol] = '\0';
			substrings++;
			destinationcol = 0;
		}
		else
		{
			pdestination[substrings][destinationcol] = *psource;
			destinationcol++;
		}
		psource++;
	}
	if (*psource == '\0')
	{
		pdestination[substrings][destinationcol] = '\0';
	}
}

void printString(char string[])
{
	while (*string != '\0')
	{
		printf("%c",*string);
		string++;
	}
	printf("\n");
}

int main()
{
	char string[100] = "wdwdwddeefefrr wdeweeedeerf\nededeede.dedeededede?efnefwekwmdemwe!ddd";
	//char *p[SUBSTRINGS_MAX];
	info oinfo;
	for (int i =0; i < SUBSTRINGS_MAX;i++ )
	    oinfo.pNum[i] = (char *)malloc((int)strlen(string) * sizeof(char));

	//printString(string+6);
	getSubString(oinfo.pNum,string," \n.?!");
	for (int i = 0; i < SUBSTRINGS_MAX; i++)
	{
		printf("p[%d] = %s\n",i, oinfo.pNum[i]);
		free(oinfo.pNum[i]);
	}
	char ch = 'F';
	char str[] = "ABCDEF";
	printf("checkCharacterInString(%c,%s) = %d\n",ch,str,checkCharacterInString(ch,str));
	return 0;
}	