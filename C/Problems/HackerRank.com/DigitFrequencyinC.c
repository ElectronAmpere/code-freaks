/**
 * @defgroup   DIGITFREQUENCYINC Digit Frequencyin C
 *
 * @brief      This file implements Digit Frequencyin C.
 *
 * @author     Vigneshrajarathinam
 * @date       2020
 */
 /**
  * { item_description }
  */
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main() {

    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    char *word;
    word = malloc(1024 * sizeof(char));
    scanf("%[^\n]", word);
    int length = (int)strlen(word);
    word = realloc(word, length + 1);
    char numberArray[10] = {'0','1','2','3','4','5','6','7','8','9'};
    int *characterCounter = (int*) malloc(length * sizeof(int));
    for (int instance = 0; instance < length; instance++)
    {
        for (int jinstance = 0; jinstance < 10; jinstance++)
        {
            if (*(word+instance) == numberArray[jinstance])
            {
                *(characterCounter+jinstance) += 1;
            }
            else
            {
                *(characterCounter+jinstance) += 0;
            }
        }
    }
    for (int instance = 0; instance < 10; instance++)
    {
        printf("%d ",*(characterCounter+instance));
    }

    return 0;
}
