/**
 * Comment Lines:
 * ----
 *
 * Comment lines are present in the program to assess the code written by
 * the programmer. Later to development of the program itself some random
 * person who's reviewing the code would easy understand code's intention.
 * These comment lines doesn't affect the program in any way if properly
 * terminated, like this one.
 * In the below comments we'll be using single line comments for knowing
 * about the types of comments available in C++ language, the reason is that
 * the multi-line should not contain the end temination before the actual end
 * of comment. So only the single comment is suited to show you the list
 * below.
 *
 * Note:
 *  Some compilers might not support comments within comments will output a
 *  warning but if you're not bothered about that then you can leave it.
 */
 // There are two types of comment statements available
 // Single line comment: //comment here
 // Multi-line comment: /* */
#include <iostream>

/**
 * Function Definition Syntax:
 * ----
 *
 * The basic syntax of any function that's written or yet to be is given by
 * return_datatype function_name (datatype1 argument1,datatype2 argument2,datatype3 argument3,...)
 * { start_of_function
 *    //comments
 *    statements are written here; statement_termiation
 *    returns;
 * } end_of_function
 *
 * If your wondering what does this all mean see the code below you might
 * understand.
 */
/**
 * @brief      This is called the main function of the program. It is assumed
 *             that the program starts here generally. But that is not the case
 *             in every system, in embedded system the actual code starts some
 *             other place than the main function which is congfigured in the
 *             file starup file startup.s which we'll be analysing later.
 *             The code below is called a function it has arguments as void and
 *             returns an integer.
 *
 *             The function main is said to display the information/message
 *             "Your First program" when executed/run
 *
 * @fn         int main(void)
 *
 * @param      void  The void
 *
 * @return     Returns the value zero at the end of the function execution
 *             Apparently, any value other than zero results in a error code
 *             which will also dealt with later.
 *
 * @error     error: 'main' must return 'int'
 *                void main(void)
 *            If the int main (void) or int main () is replaced with
 *            void main (void) or any other datatype is replies with an error
 *            like the above error:  statement.
 */
int main(void)
{
	/**
	* Variable:
	* ----
	*
	* A variable is location to store information necessary to process in a
	* given program.
	* The variables declared in the function definition will only be accessable
	* only within that function. If accessed compilation errors will occur.
	*/
	int int_variable1,int_variable2,int_conditionresult;

	std::cout<<"Enter integer value1: ";
	std::cin>>int_variable1;
	std::cout<<"Enter integer value2: ";
	std::cin>>int_variable2;

	/**
	* If - else statement:
	* ----
	*
	* The if-else statement is pretty much self explanatory, there is an if
	* condition which must be held true in order to enter the statement or
	* else, the else statement.
	*
	* Syntax:
	* ----
	*
	* if-else:
	* -----
	*
	* if (relational condition only)
	* {
	*   statements;
	*   .
	*   .
	*   .
	* }
	* else
	* {
	*   statements;
	*   .
	*   .
	*   .
	* }
	*
	* if-else if-else;
	* ----
	*
	* if (relational condition only)
	* {
	*   statements;
	*   .
	*   .
	*   .
	* }
	* else if (relational condition only)
	* {
	*   statements;
	*   .
	*   .
	*   .
	* }
	* else
	* {
	*   statements;
	*   .
	*   .
	*   .
	* }
	*
	* Note:
	*  The if statement can be used alone but the else cannot exists alone
	*  without an if statement.
	*  The if statement can also be proceeded by an else if statement.
	*
	*/
	/**
	* Here, the condition that's checked by the if statement is whether the
	* int_variable1 is greater than or equal to int_variable2 and in the else
	* statement will be implied that int_variable2 is greater than int_variable1
	* so the below statement make sense.
	*/
	if (int_variable1 >= int_variable2)
	{
		int_conditionresult = int_variable1 - int_variable2;
	}
	else
	{
		int_conditionresult = int_variable2 - int_variable1;
	}

	std::cout<<"Result:"<<int_conditionresult<<"\n";

	std::cout<<"Enter dividend: ";
	std::cin>>int_variable1;
	std::cout<<"Enter divisor: ";
	std::cin>>int_variable2;

	/**
	* From the previous statements the below will be an example to if-else if
	* and so on ...
	* The below example may not make sense but guess what it's an example so
	* don't bother.
	*/
	if (int_variable2 == 1)
	{
		std::cout<<"Result:"<<int_variable1<<"\n";
	}
	else if (int_variable2 == 2)
	{
		std::cout<<"Result:"<<int_variable1/int_variable2<<"\n";
	}
	else if (int_variable2 == 3)
	{
		std::cout<<"Result:"<<int_variable1/int_variable2<<"\n";
	}
	else
	{
		std::cout<<"Result:"<<int_variable1/int_variable2<<"\n";
	}

	std::cout<<"Enter a number: ";
	std::cin>>int_variable1;

	/**
	* The switch case statement:
	*
	* Syntax:
	* ----
	*
	* switch (expression)
	*  {
	*    case constant-expression:
	*
	*    { (optional-brackets)
	*
	*      statement1(s);
	*
	*    } (optional-brackets)
	*
	*    break; (optional)
	*
	*    case constant-expression:
	*
	*      statement2(s);
	*
	*    break; (optional)
	*    .
	*    .
	*    .
	*    (you can have any number of case statements)
	*
	*    default: (optional)
	*
	*      statementn(s);
	*
	*    break; (optional)
	*  }
	*/
	switch (int_variable1)
	{
		case 0:
		  std::cout<<" 0\n ";
		break;
		case 1:
		  std::cout<<" 1\n ";
		break;
		case 2:
		  std::cout<<" 2\n ";
		break;
		case 3:
		  std::cout<<" 3\n ";
		break;
		case 4:
		  std::cout<<" 4\n ";
		break;
		default:
		  std::cout<<" default\n ";
		break;
	}

	/**
	* To hold program until result is visualised
	*/
	std::cout<<"Enter number : ";
	std::cin>>int_variable1;

	/**
	* The switch case
	*/
	switch (int_variable1)
	{
		case 0:
		{
		  std::cout<<" 0\t ";
		  switch (int_variable2)
		  {
		    case 0:
		      std::cout<<" 0\n ";
		    break;
		    case 1:
		      std::cout<<" 1\n ";
		    break;
		    default:
		      std::cout<<" default\n ";
		    break;
		  }
		}
		break;
		case 1:
		  std::cout<<" 1\t ";
		break;
		case 2:
		  std::cout<<" 2\t ";
		break;
		case 3:
		  std::cout<<" 3\t ";
		break;
		case 4:
		  std::cout<<" 4\t ";
		break;
		default:
		  std::cout<<" default\n ";
		break;
	}

	/**
	* To hold program until result is visualised
	*/
	std::cout<<"Enter number : ";
	std::cin>>int_variable1;

	/**
	* The below statement has a single-line condition statement
	*
	* Syntax:
	* ----
	*
	* Terinary Conditional Statement:
	* ----
	*
	* (relational condition only)?statement1:statement2;
	*
	* (check relational condition only)?if true execute this:else execute this;
	*
	* Note:
	* ----
	*
	* This can also be nested like the others
	*
	* Example:
	* ----
	* (if condition1 true)?(if condition2 is true)?if condition2 true execute this :else execute this :else if condition1 false execute this;
	*  |                                |             ^                  ^                 ^                            ^
	*  |    if true will come here      |             |                  |                 |                            |
	*  ---------------------------------|--------------                  |                 |                            |
	*  |                                |                                |                 |                            |
	*  |                                |        if false will come here |                 |                            |
	*  ---------------------------------|--------------------------------|-----------------|-----------------------------
	*                                   |    if true will come here      |                 |
	*                                   ----------------------------------                 |
	*                                   |                                                  |
	*                                   |            if false will come here               |
	*                                   ----------------------------------------------------
	*
	*/
	std::cout<<((int_variable1 > 0)?int_variable1:0xFF);

	/**
     * This is the non-essential return statement for the function when specified
     * the return has to be 0 if not then you just don't care enough.
     */
    return (0);
}